function [vxy] = handv(fi,dfi,l)
% function out = handv(fi,l)
% compute endpoint velocity given sho/elb (fi and dfi) and segment lengths l(1)
% and l(2).
% fi and dfi must be time vertical axis.
% external coordiante frame fi.
for i =1:size(fi,1)
    vecfi = fi(i,:)';
    
    Jac = [-l(1)*sin(vecfi(1)), -l(2)*sin(vecfi(2));
            l(1)*cos(vecfi(1)), l(2)*cos(vecfi(2))];

    vecdfi = dfi(i,:)';
    vxy(i,:) =(Jac*vecdfi)';
end
