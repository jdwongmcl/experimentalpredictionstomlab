function fi=cart2polar(p,l)
%purpose, to get from 2DOF cartesian coordinates to 2DOF polar coordinates
%input:     p=[x;y];  end position of segement 2 and l=[l1;l2] segment lengths
%output:    fi=[fi1;fi2];
%biased for top view right arm.
%DAKistemaker 07-2008

a=size(p);
% if a(1)<a(2)&a(2)~=2|a(2)==1;
%     p=p';a=fliplr(a);
% end

fi=zeros(a(1),2);
for i=1:a(1)
    pc=p(i,:);
    beta=atan2(pc(2),pc(1));
    alpha=acos( (l(2)^2-l(1)^2-norm(pc)^2)/(-2*norm(pc)*l(1)) );
    fi(i,1)=beta-alpha;
    s=[l(1)*cos(fi(i,1)) l(1)*sin(fi(i,1))];
    pcs=pc-s;
    fi(i,2)=mod(atan2(pcs(2),pcs(1)),2*pi);
end