function [stimdes, qdes, gammades, lcereldes, Fdes, Ttot] = calc_stable_start2(fi,F,angle,lseg)
% purpose: to find STIM, q, gamma and lcerel that accompanies the least squares
% equilibrium muscle forces (e.g. at the start of an optimization)
% with a particular force acting on the hand and (angle of the field of) gravity.
% inputs: fi (desired angle)
%         F (forces acting on the hand)
%         angle (angle of gravity: 0 means no gravity, pi/2 means "full" gravity)
% output: stimdes, qdes, gammades, lcereldes, Fdes, Text; these are the
%         (steady state) STIM,q,gamma and lcerel that lead to the required Fce (Fdes)
%         countering the sum of external and gravitational forces (Ttot).
%
% DA Kistemaker 2014. Last edits 2016 to incorporate gravitational forces/torques.
% jdw edited to update to gaussian FISOM equations to match optimization call.
% perhaps this function could be taken in as a handle...

load params_TOMLAB %jdw: this is bad form and creates illegible code.
if angle>(2*pi)
    disp('Are you sure the input angle is in radians? Hit any key to continue...')
    pause
end

g=-9.81*sin(angle);
l = lseg;
F=F(:);

Jinv = [-l(1)*sin(fi(1))-l(2)*sin(fi(2)) l(1)*cos(fi(1))+l(2)*cos(fi(2));
    -l(2)*sin(fi(2))                 l(2)*cos(fi(2))];
Text = Jinv*F;

Jinv1 = [0      d(1)*cos(fi(1));
    0      0];
Tgrav1= Jinv1*[0;m(1)*g];

Jinv2 = [0      l(1)*cos(fi(1))+d(2)*cos(fi(2));
    0      d(2)*cos(fi(2))];
Tgrav2 = Jinv2*[0;m(2)*g];

Ttot = Text+Tgrav1+Tgrav2;

fijo= [fi(1)-pi/2 fi(2)-fi(1)]; % joint angles
rloi=zeros(1,nmus)+rmapar(1,:)+rmapar(2,:).*fijo(1)+rmapar(3,:).*fijo(1)^2;
rmomarm1=rmapar(2,:)+2*rmapar(3,:)*fijo(1);
rloi=rloi+rmapar(4,:)+rmapar(5,:).*fijo(2)+rmapar(6,:).*fijo(2)^2; % muscle length
rmomarm2=rmapar(5,:)+2*rmapar(6,:)*fijo(2);
momarm=[rmomarm1;rmomarm2]; % moment arms


Fmin=1e-2; %lower bound on normalized gaussian muscle forces %this parameter is  also used in calc_stable_start!!! Adjust accordingly!!!

Fdes = fmincon(@LSQ_Fmus,ones(1,6)*10,[],[],momarm,Ttot,Fmin.*gmax);
mustor=(ones(nseg,1)*Fdes).*momarm;
tor=-sum(mustor');

% gse = ifThenElse(ese<0,(b_Fse/.1)*ese + b_Fse, kse.*ese.^2+b_Fse,0.00005);
ese  = sqrt((Fdes-1)./kse);
lse   = rlsenul+ese

lce   = rloi - lse;
lcerel= lce./rlceopt;

lcereldes = lcerel;
% 1st order gaussian approximation of parabola
% parm_a=0.447241511579016;
% fisomrel = exp(-((lcerel-1)/(parm_a(1))).^(2));
% fisomrel = (1-Fmin)*(fisomrel/1)+Fmin;

% old dinant parabolic fisomrel. 
% c=[-3.188775510204081   6.377551020408163  -2.188775510204081];
% fisomrel=ifThenElse((c(1).*lcerel.^2+c(2).*lcerel+c(3)),'gt',ones(size(lcerel))*Fmin,(c(1).*lcerel.^2+c(2).*lcerel+c(3)),ones(size(lcerel))*Fmin,0.03); % relative isometric muscle force

c_opt2_56 = [0.406204464574880   0.447882024677048]; %values optimized for w=.56
c_opt2_66 = [0.479024092010275   0.527890189814057]; %values optimized for w=.66
c1_gaus   = [c_opt2_56(1) c_opt2_56(1) c_opt2_66(1) c_opt2_66(1) c_opt2_66(1) c_opt2_66(1)];
c2_gaus   = [c_opt2_56(2) c_opt2_56(2) c_opt2_66(2) c_opt2_66(2) c_opt2_66(2) c_opt2_66(2)];

fisomrel = ((1-Fmin)/2) *( exp(-((lcerel-1)./c1_gaus).^(2)) + exp(-((lcerel-1)./c2_gaus).^(4))) +Fmin;


qdes = Fdes./(fisomrel.*gmax);

a_act=-7; a1_act=log10(exp(a_act));
kCa=0.8e-5;
gamma_0=.00001;
b_act=[5.0717 1.0888 -0.2014];
B_act=b_act(1)+b_act(2)*lcerel+b_act(3)*lcerel.^2;

gammades =       (exp(log(((1-q0)./(qdes - q0) -1) ./exp(a_act*B_act))/a1_act))/kCa;

stimdes = (gammades-gamma_0)./(1-gamma_0);



function error=LSQ_Fmus(Fmus)

error = sum(Fmus.^2);
