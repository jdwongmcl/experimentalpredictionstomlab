warning off
% ca
%% Musculoskeletal model of the arm
% (2DOF segmental dynamics, actuated by 6 Hill-type muscles and activation
% dynamics according to Hatze.

load params_TOMLAB

l=[.315 .421];
fi_targets=cart2polar([-.18 .22; -.18 .52],l); % calculate start and target angles

nn =        [10 20 20 30 40 45 50]; %twice 20 is because the 2nd 20 calculation inludes q=f(gamma)

for grav_angle = [0:15:90]*pi/180;
    for direction = [0:1]
        if direction == 0
            fi_start  = fi_targets(1,:);
            fi_target = fi_targets(2,:);
        else
            fi_start  = fi_targets(2,:);
            fi_target = fi_targets(1,:);
        end
        %check on start position of hand
        hand_start=[sum(l.*cos(fi_start)) sum(l.*sin(fi_start))];
        hand_target=[sum(l.*cos(fi_target)) sum(l.*sin(fi_target))];
        %% calculating initial states etc...
        F=[0 0]; %no external forces acting on hand
        [stim_start, q_start, gamma_start, lce_start, Fdes, Ttot] = calc_stable_start2(fi_start,F,grav_angle,l);
        disp('calculated equilibrium start... now running OC problem')
        for i = 1:length(nn)
            if i==1
                tic
            end
            load params_TOMLAB
            
            l=[.315 .421];   % overrule segment length, now includes ~length of forearm plus hand
            gamma_0=.00005;
            steps=nn(i);
            toms t
            time =.45;
            p = tomPhase('p', t, 0, time, steps, [], 'spline3');
            setPhase(p);
            
            fi = tomState('fi',1,2);        % segment angles
            fid = tomState('fid',1,2);      % segment angular velocities
            lcerel = tomState('lcerel',1,6);% relative contractile element length
            gamma = tomState('gamma',1,6);  % relative amount of intracellular [Ca++] concentration
            stim = tomControl('stim',1,6);  % relative muscle activation
            alpha = tom('alpha',1,1);
            %% initial guess
            if i<2
                
                pause;
                x0 = {
                    icollocate({
                    %fi  == vec(interp1([0 time/2 time],[fi_start;(fi_start-fi_target)/2; fi_target],t))
                    fi == (fi_start-fi_target)/2
                    fid  == (fi_target-fi_start)/time
                    lcerel  == lce_start
                    gamma == gamma_start
                    })
                    collocate({
                    stim  == stim_start %relative muscle activation
                    alpha == 1;
                    })
                    };
            else
                x0 = {icollocate({
                    fi == fiopt
                    fid == fidopt
                    lcerel == lceopt
                    gamma == gammaopt
                    alpha == alphaopt
                    })
                    
                    collocate({
                    stim == stimopt
                    })
                    };
            end
            
            %% Boundary constraints
            cbnd = {
                initial({
                fi == fi_start;
                fid == 0;
                lcerel == lce_start;
                gamma == gamma_start;
                %             stim == stim_start
                })
                final({fi == fi_target;
                fid == 0;
                dot(fid)==0;
                })
                };
            
            %% Box constraints
            cbox = {
                mcollocate(0 <= fi(1) <= pi/1.5)
                mcollocate(0 <= fi(2)-fi(1) <= pi)
                mcollocate(-100 <= fid <= 100)
                mcollocate(1-width <= lcerel <= 1+width)
                mcollocate(gamma_0 <= gamma  <= 1)
                0.005 <=  collocate(stim)  <= 1
                0 <= collocate(alpha) <=1
                };
            
            disp(['Round: ',num2str(i)])
            
            %%%
            %% ODEs and path constraints
            
            %% calculate muscle length and moment arms
            % total muscle length (loi) is a 2nd order polynomial depending on joint angle (fi)
            % moment arm of muscle = dloi/dfi
            % 30-11-2010 Some recoding to avoid loops.
            fijo= [fi(1)-pi/2 fi(2)-fi(1)]; % joint angles
            rloi=zeros(1,nmus)+rmapar(1,:)+rmapar(2,:).*fijo(1)+rmapar(3,:).*fijo(1)^2;
            rmomarm1=rmapar(2,:)+2*rmapar(3,:)*fijo(1);
            rloi=rloi+rmapar(4,:)+rmapar(5,:).*fijo(2)+rmapar(6,:).*fijo(2)^2; % muscle length
            rmomarm2=rmapar(5,:)+2*rmapar(6,:)*fijo(2);
            rmomarm=[rmomarm1;rmomarm2]; % moment arms
            
            %%%
            %% Musculosketal dynamics
            
            %% New Activation Dynamics
            a_act=-7; a1_act=log10(exp(a_act));
            kCa=0.8e-5;
            gamma_0=.00001;
            b_act=[5.0717 1.0888 -0.2014];
            B_act=b_act(1)+b_act(2)*lcerel+b_act(3)*lcerel.^2;
            gammad=rm.*(stim*(1-gamma_0)-gamma +gamma_0);
            
            if i<3 %&& warmstart==0
                q=gamma;
                alpha=1;
            else
                q = (1-alpha) * (q0 + (1-q0) ./ (1 + ((kCa*gamma).^(a1_act).*exp(a_act*B_act)))) + alpha*gamma;
            end
            
            
            
            %% Force-length relationship
            % Fse
            lse=rloi-lcerel.*rlceopt; %tendon length
            ese=lse-rlsenul; % tendon elongation
            b_Fse = 1;
            gse = ifThenElse(ese<0,(b_Fse/.1)*ese + b_Fse, kse.*ese.^2+b_Fse,0.00005);
            
            % Fisomrel, gaussian functions %see also Adjusted Fce lce relationship.doc and newFcelce.m in NewHill 2013 folder
            % 2nd order Gaussian approximation has a relativce force of ~.12 at 1-width
            
            Fmin=1e-2; %this parameter is  also used in calc_stable_start!!! Adjust accordingly!!!
            c_opt2_56 = [0.406204464574880   0.447882024677048]; %values optimized for w=.56
            c_opt2_66 = [0.479024092010275   0.527890189814057]; %values optimized for w=.66
            c1_gaus   = [c_opt2_56(1) c_opt2_56(1) c_opt2_66(1) c_opt2_66(1) c_opt2_66(1) c_opt2_66(1)];
            c2_gaus   = [c_opt2_56(2) c_opt2_56(2) c_opt2_66(2) c_opt2_66(2) c_opt2_66(2) c_opt2_66(2)];
            fisomrel = ((1-Fmin)/2) *( exp(-((lcerel-1)./c1_gaus).^(2)) + exp(-((lcerel-1)./c2_gaus).^(4))) +Fmin;
            
            % fce
            fce=gse;
            fcerel=fce./gmax; % actual relative muscle force
            
            %% Force-velociy relationship
            %smooth version of KvS brel(q)
            vfactmin=0.1;
            q0_b = (log(1/vfactmin-1)+q0*22)/22;
            brel = brel./(1+exp(-22*(q-q0_b)));
            
            % simplified arel
            arel = arel.*fisomrel;
            
            dvdf_isom_con=brel./(q.*(fisomrel+arel)); % slope in the isometric point at wrt concentric part
            dvdf_isom_ecc=dvdf_isom_con./slopfac; % slope in the isometric point at wrt eccentric part
            dFdvcon0=1./dvdf_isom_con;
            s_as = 1./sloplin;
            p1 = -(fisomrel.*q.*(fasymp - 1))./(s_as - dFdvcon0.*slopfac);p3 =  -fasymp.*fisomrel.*q;
            p2 =  (fisomrel.^2.*q.^2.*(fasymp - 1).^2)./(s_as - dFdvcon0.*slopfac);p4 =  -s_as;
            %smooth full KvS version
            vce_softness = .001;
            lcereld_c = ifThenElse((fcerel./q) <= fisomrel,brel.*(fcerel-q.*fisomrel)./(fcerel+q.*arel),0,vce_softness);
            lcereld_c = lcereld_c + ifThenElse(dvdf_isom_con>sloplin,sloplin.*(fcerel-q.*fisomrel),0,vce_softness);
            lcereld_e = ifThenElse((fcerel./q) > fisomrel,(-(fcerel + p3 + p1.*p4 + (fcerel.^2 - 2*fcerel.*p1.*p4 + 2*fcerel.*p3 + p1.^2.*p4.^2 - 2*p1.*p3.*p4 + p3.^2 + 4*p2.*p4).^(1/2))./(2.*p4)),0,vce_softness);
            lcereld_e = lcereld_e + ifThenElse(dvdf_isom_ecc>(sloplin./slopfac),(sloplin./slopfac).*(fcerel-q.*fisomrel),0,vce_softness);
            vcerel = lcereld_c + lcereld_e;
            %When setting slopfac to 1, these functions reduce to:
            %     lcereld_nonl    = ifThenElse ((fcerel./q) <= fisomrel,(brel.*(fcerel-q.*fisomrel)./(fcerel+q.*arel)) , (-(fcerel + p3 + p1.*p4 + (fcerel.^2 - 2*fcerel.*p1.*p4 + 2*fcerel.*p3 + p1.^2.*p4.^2 - 2*p1.*p3.*p4 + p3.^2 + 4*p2.*p4).^(1/2))./(2.*p4)),vce_softness);
            %     vcerel          = ifThenElse(dvdf_isom_con>sloplin,sloplin.*(fcerel-q.*fisomrel),lcereld_nonl,vce_softness);
            %or:
            %     lcereld_c    = ifThenElse ((fcerel./q) <= fisomrel,(brel.*(fcerel-q.*fisomrel)./(fcerel+q.*arel)) , 0 , vce_softness);
            %     lcereld_e    = ifThenElse ((fcerel./q) > fisomrel,-(fcerel + p3 + p1.*p4 + (fcerel.^2 - 2*fcerel.*p1.*p4 + 2*fcerel.*p3 + p1.^2.*p4.^2 - 2*p1.*p3.*p4 + p3.^2 + 4*p2.*p4).^(1/2))./(2.*p4),vce_softness);
            %     vcerel       = ifThenElse(dvdf_isom_con>sloplin,sloplin.*(fcerel-q.*fisomrel),lcereld_c+lcereld_e,vce_softness);
            
            
            % end of muscle dynamics
            
            %% Calculation muscle torques
            mustor=(ones(nseg,1)*gse).*rmomarm;
            tor=-sum(mustor');
            
            %% Skeletal Dynamics
            % dynamics of double pendulum, including gravity and
            % external forces on the hand/wrist (e.g. simulated robotic forces).
            % avoiding (slow) matrix calculations.
            mb = 0; %mass of robotic manipulandum
            g=9.81*sin(grav_angle); % gravity w.r.t. angle of chair
            Fx=0;Fy=0; % no external forces on the hand.
            
            
            %% Using Lagrangian Dynamics
            a_b=fi(1)-fi(2);
            a1=(m(1)*d(1)*d(1) + m(2) * l(1)*l(1)) + I(1) + mb*l(1)^2;
            a2=m(2)*l(1)*d(2)*cos(a_b) + mb*l(1)*l(2)*cos(a_b);
            a3=m(2)*l(1)*d(2)*cos(a_b) + mb*l(1)*l(2)*cos(a_b);
            a4=m(2)*d(2)*d(2) + mb*l(2)*l(2) +I(2);
            
            b1=(-1) * m(1)*g*d(1)*cos(fi(1)) - m(2)*g*l(1)*cos(fi(1)) + Fy*l(1)*cos(fi(1)) - Fx*l(1)*sin(fi(1)) - m(2)*l(1)*d(2)*sin(a_b)*fid(2)*fid(2) - mb*l(1)*l(2)*sin(a_b)*fid(2)*fid(2) - mb*g*l(1)*cos(fi(1)) + tor(1) - tor(2);
            b2=(-1) * m(2)*g*d(2)*cos(fi(2))                          + Fy*l(2)*cos(fi(2)) - Fx*l(2)*sin(fi(2)) + m(2)*l(1)*d(2)*sin(a_b)*fid(1)*fid(1) + mb*l(1)*l(2)*fid(1)*fid(1)*sin(a_b) - mb*g*l(2)*cos(fi(2)) + tor(2);
            %%
            
            %% End of musculosketelal dynamics
            
            %% Equality contraints
            ceq = collocate({
                dot(fi) == fid
                [a1 a2; a3 a4]*dot(fid') == [b1; b2]
                dot(lcerel) == vcerel
                dot(gamma)== gammad
                });
            
            
            %% Objectives
            %     Pmus = gse.*vcerel.*rlceopt;
            k = [1 1 1 2 3 3.5 4 4.5];
            %         objective = k(i)*(integrate(sum(stim.^2)+sum(.002*dot(stim).^2)))+alpha^2;
            objective = 1000*integrate(sum(stim.^2))+alpha^2;
            %% Solve the problem
            options = struct;
            options.name = '2DOF arm model with 6 Hill-type muscles.m';
            %options.scale = 'auto';
            OPTIONS.PriLev = 2;
            options.Prob.SOL.optPar(35) = 100000;
            options.Prob.SOL.optPar(30) = 200000;
            Prob = sym2prob(objective, {cbox, cbnd, ceq}, x0, options);
            result = tomRun('snopt',Prob,1);
            solution = getSolution(result);
            
            fiopt = subs(fi, solution);
            fidopt = subs(fid, solution);
            lceopt = subs(lcerel, solution);
            gammaopt = subs(gamma, solution);
            stimopt = subs(stim, solution);
            alphaopt= subs(alpha,solution);
            F = subs([Fx Fy],solution);
            s=[fi(1) fid(1) fi(2) fid(2)];
            
        end
        ang = grav_angle*180/pi;
        f_k=result.f_k;
        Exit=[result.ExitFlag result.Inform];
        cd figures
        eval(['save testl', num2str(ang),'_',num2str(direction),' tor mustor stim t time steps s fi fid gse fce lcerel q lse rlsenul rloi objective alpha f_k Exit solution'])
        cd ..
        
    end
end

%%
t_plot = linspace(0,time,451)'; % Time vector where we want our trajectory evaluated.
u_plot = collocate(t);
% figure(1);
s_plot = atPoints(t_plot,subs(s,solution));
stim_u = atPoints(u_plot,subs(stim,solution));
fi=s_plot(:,[1 3]);
fip=s_plot(:,[2 4]);
% [e,h]=showmov3(fi,l,s_plot,0);
% plotvs(h)
% axis equal
figure
plot(u_plot,stim_u)
tor = atPoints(t_plot,subs(tor,solution));
t=t_plot;
state = [fi fip];
%%

  