function [out,elb] = handxy(fi,l)
% function out = handxy(fi,l)
% compute endpoint location given sho/elb angles and segment lengths l(1)
% and l(2).
% fi must be time vertical axis.
% external coordiante frame fi.
for i =1:size(fi,1)
    elb(i,:) = [l(1)*cos(fi(i,1)),l(1)*sin(fi(i,1))];
    out(i,1) = elb(i,1) + l(2)*cos(fi(i,2));
    out(i,2) = elb(i,2) + l(2)*sin(fi(i,2));
end
