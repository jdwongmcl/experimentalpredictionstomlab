function out = snoptDoublePendulumForceRateAndWorkFixedTime(xybeg,xyend,tdur,pIn,cForceRateRate,nameLabel)
% function out = snoptDoublePendulumForceRateAndWorkFixedTime(xybeg,xyend,tdur,pIn,cForceRateRate,nameLabel)
%cForceRateRate = 3.5e-4; %3.5e-6 puts force rate cost in the ballpark BUT if that is the coef then wed have too high f-rate costs probably...
        % 3.5e-5 or greater gives bell-shaped velocity profiles...

        if ~isfield(pIn,'Objective')
            fprintf('default objective: forceRate.\n');
            pIn.Objective = 'forceRate';
        end
cObj=.0001;

warning off

l1 = pIn.l(1);
l2 = pIn.l(2); %matches the VU simulations for L1 and L2 arm length. of course the masses aren't matched.
% load params_TOMLAB

% FROM JDW CLUFF KUO, the target locations and durations.
% q_targets = [62.0000   87.0000
%   66.3376   82.6624
%   68.6401   80.3599
%   70.0806   78.9194
%   70.9822   78.0178].*(pi/180);
% sTimeExptHalfCycle =[0.8571    0.6451    0.5173    0.4286    0.3681];

% harris wolpert style replication
x=xybeg(1);y=xybeg(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);

q_start = [q1,q2];

x=xyend(1);y=xyend(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);
q_target = [q1,q2];


iTGT = 1;% there is only one target
%% calculating initial states etc...
F=[0 0]; %no external forces acting on hand
stim_start=[0,0];
disp('calculated equilibrium start... now running OC problem')
stepLevel = 10;
for iSteps = 1:length(stepLevel) %
  if iSteps==1
    tic
  end
  disp(['Round: ',num2str(iSteps)])
  
  %%%setup the tomlab problem by defining the
  %%%1 TIME
  %%%2 PHASE
  %%%3 CONTROLS
  %%%4 STATES
  toms t %
  tend = tdur;
  steps=10;
  p = tomPhase('p', t, 0, tend, stepLevel(iSteps), [], 'spline3');
  setPhase(p);
  q = tomState('q',1,2);        % segment angles
  dqdt = tomState('dqdt',1,2);      % segment angular velocities
  stim = tomControl('stim',1,2);  % jer: torque
  %%%/setup the tomlab problem by defining the
  
  %%% initial guess
  if iSteps==1
    %initial guess
    x0 = {
      %tend == 1 % WARNING: if T IS AN OPT VARIABLE, then THIS NEEDS TO BE FIRST IN LIST. propt documentation.
      icollocate({
      q  == (q_start-q_target)/2;%vec(interp1([0 time],[q_start; q_target],t))'
      dqdt  == (q_target-q_start)/1;%jdw hack
      })
      collocate({
      %stim  == stim_start %relative muscle activation
      %         stim   == [vec(interp1([0 time],[4.5; 4.51],t/2))',vec(interp1([0 time],[-4.5; -4.51],t/2+1))']
      })
      };
    %%% otherwise warmstart
  else
    x0 = {
      %tend == tendopt %%here we are not solving for this!!
      icollocate({
      q == qopt
      dqdt == qdopt
      })
      
      collocate({
      stim == stimopt
      })
      };
  end
  
  %%% Boundary constraints
  cbnd = {
    initial({
    q == q_start;
    dqdt == 0;
    stim == stim_start;
    })
    final({q == q_target;
    dqdt == 0;
    dot(dqdt)==0;
    stim == stim_start;
    })
    };
  
  %%% Box constraints
  cbox = {
    %       mcollocate(0 <= q(1) <= 3*pi/2)
    %       mcollocate(-pi/4 <= q(2)-q(1) <= pi)
    %mcollocate(-100 <= qd <= 100)
    
    %0.005 <=  collocate(stim)  <= 1
    %0 <=tend <=10
    };
  %%%
  % ODEs and path constraints via virtual power
  
I1 = pIn.I(1);
I2 = pIn.I(2);
m1 = pIn.m(1);
m2 = pIn.m(2);
r1 = pIn.r(1);
r2 = pIn.r(2);

MassMat = [ I1 + r1^2*m1 + l1^2*m2, l1*cos(q(1) - q(2))*(r2*m2);
  l1*cos(q(1) - q(2))*(r2*m2),          m2*r2^2 + I2];

F =  [-l1*dqdt(2)^2*sin(q(1) - q(2))*(r2*m2);
  l1*dqdt(1)^2*sin(q(1) - q(2))*(r2*m2)];

dampingCoef = pIn.dampCoef;
tauDamping = dampingCoef*[dqdt(1);dqdt(2)];

XYJac = [ -l1*sin(q(1)), -l2*sin(q(2));
          l1*cos(q(1)),  l2*cos(q(2))];
 
        Vhand = XYJac*dqdt';


  ceq = collocate({
    dot(q') == dqdt'
    MassMat*dot(dqdt') == (F + [stim(1)-stim(2);stim(2)] + [tauDamping(1)-tauDamping(2);tauDamping(2)]);
    });
  
  %%% Objective
  
  
  %%% Work cost
  powerSho = stim(1)*dqdt(1) - stim(2)*dqdt(1);
  powerElb = stim(2)*dqdt(2);
  posPowerSho = integrate((powerSho+abs(powerSho))/2);
  posPowerElb = integrate((powerElb+abs(powerElb))/2);
  costPositiveWork = posPowerSho + posPowerElb;
  kMechanicalWorkEfficiencyMargaria = 1/0.25;
  costPositiveWork = costPositiveWork*kMechanicalWorkEfficiencyMargaria;
  
  %%% Force rate cost
  nnshift = 0.00001;
  
  costForceRateQuad = cForceRateRate*sum(integrate((dot(dot(stim'))).^2+nnshift));
  costForceRateLin = cForceRateRate*sum(integrate(sqrt((dot(dot(stim'))).^2+nnshift)));
  costForceRate = costForceRateLin;
  
  %%% Resting cost
  restingMetRate = 100;%100 Watts.
  costResting = sqrt(tend^2+0.00001)*restingMetRate;
  nameOpt = 'fixedTimeHW';
  
  costJerk = sum(integrate(dot(dot(Vhand)).^2));
  costTorqueRate = sum(integrate(dot(stim').^2));
  
  %%% Reward cost
  reward = 0;%cReward * sqrt(tend^2+0.00001); %avoiding tend<0?
  objective = cObj * (costForceRate + costPositiveWork + reward);
  
  switch pIn.Objective
      case 'forceRate'
          % above, the obj was already frate and work.
          fprintf('optimizing for force rate and work.\n');
      case 'jerk'
    
      objective = 0.00001*costJerk;
      fprintf('optimizing for jerk.\n');
      pause;
      case 'torqueRate'
          objective = 0.0001*costTorqueRate;
          fprintf('optimizing for torque rate.\n');
  end
  
  nameSim='Kinarm';
  %% Solve the problem
  options = struct;
  options.name = [nameSim,' ',nameOpt,'.m'];
  %       options.scale ='auto';
  
  %%% optimization setup
  options.PriLev = 2;
  options.Prob.SOL.optPar(35) = 100000; %iterations limit
  options.Prob.SOL.optPar(30) = 200000; %major iterations limit
  Prob = sym2prob(objective, {cbox, cbnd, ceq}, x0, options);
  result = tomRun('snopt',Prob,1);
  solution = getSolution(result);
  qopt = subs(q, solution);
  qdopt = subs(dqdt, solution);
  stimopt = subs(stim, solution);
  
end %end dampingLevel loop

%%%add to solution vector
solutions(iTGT) = solution;
%%%get values
ECostForceRate(iTGT) = subs(costForceRate,solution);
ECostPositiveWork(iTGT) = subs(costPositiveWork,solution);
ECostResting(iTGT) = subs(costResting,solution);
EReward(iTGT) = subs(reward,solution);

EdotCostForceRate(iTGT) = subs(costForceRate,solution)./tend;
EdotCostPositiveWork(iTGT) = subs(costPositiveWork,solution)./tend;
EdotCostResting(iTGT) = subs(costResting,solution)./tend;
EdotReward(iTGT) = subs(reward,solution)./tend;
times(iTGT) = tend;
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('TARGET %i\n',iTGT);
fprintf('Fdotdot power = %.4f \n',EdotCostForceRate(iTGT));
fprintf('Avg positive power = %.4f\n',EdotCostPositiveWork(iTGT));
fprintf('Resting power = %.4f\n',EdotCostResting(iTGT));
fprintf('Reward Power = %.4f\n',EdotReward(iTGT));

s=[q(1) q(2) dqdt(1) dqdt(2)];
ang = 0;%grav_angle*180/pi;
f_k=result.f_k;
Exit=[result.ExitFlag result.Inform];

%%
t_plot = linspace(0,subs(tend,solution),451)'; % Time vector where we want our trajectory evaluated.
%u_plot = collocate(t);
% figure(1);
s_plot = atPoints(t_plot,subs(s,solution));
stim_u = atPoints(t_plot,subs(stim,solution));
qplot=s_plot(:,[1 2]);
dqdtplot=s_plot(:,[3 4]);
jacArm = @(q1,q2) [-pIn.l(1)*sin(q1), -pIn.l(2)*sin(q2); pIn.l(1)*cos(q1), pIn.l(2)*cos(q2);];
for ifwd = 1:length(s_plot)
  istate = s_plot(ifwd,:)'; %rotate to get column vector
  hand.vxy(ifwd,1:2) = (jacArm(istate(1),istate(2)) * istate(3:4))';
  hand.speed(ifwd) = sqrt(hand.vxy(ifwd,1)^2+hand.vxy(ifwd,2)^2);
  hand.t(ifwd) = t_plot(ifwd);
  hand.xy(ifwd,1:2) = [pIn.l(1)*cos(istate(1))+pIn.l(2)*cos(istate(2)),
  pIn.l(1)*sin(istate(1))+pIn.l(2)*sin(istate(2))];

end

figure(8); hold on;
plot(t_plot,hand.speed);
ylabel('Velocity (m/s)'); xlabel('Time (s)');
figure(9); hold on;
plot(t_plot,stim_u(:,1));
plot(t_plot,stim_u(:,2));
ylabel('Force (N)');xlabel('Time (s)');
tor = atPoints(t_plot,subs(stim,solution));
t=t_plot;
state = [qplot dqdtplot];
figure(10); hold on;
plot(hand.xy(:,1),hand.xy(:,2));
xlabel('Position (m)');xlabel('Position (m)');

%% plot forcerate+work
figure(11);
freqs = 1./tdur;
plot(freqs,EdotCostPositiveWork,'marker','.','markersize',20);hold on;
plot(freqs,EdotCostForceRate,'marker','.','markersize',20);
plot(freqs,EdotCostForceRate+EdotCostPositiveWork,'marker','.','markersize',20);
legend('work','frate','net');

ylabel('Power (W)');
xlabel('Frequency (Hz)');

nametot = [nameOpt,pIn.Objective,nameSim,nameLabel];
eval(['save ',nametot,' ECostResting ECostPositiveWork ECostForceRate EReward EdotCostResting EdotCostPositiveWork EdotCostForceRate EdotReward times stim t tend steps s q dqdt objective f_k Exit solution'])

toc
%%
figure(8);
savefig([nametot,'_dxdt']);
figure(9);
savefig([nametot,'_u']);
figure(10);
savefig([nametot,'_xy']);
figure(11);
savefig([nametot,'_energybreakdown']);

out = struct;
out.t_plot = t_plot;
out.s_plot = s_plot;
out.hand = hand;
out.stim = stim_u;
out.solution = solution;
out.EdotCostForceRate = EdotCostForceRate;
out.EdotCostPositiveWork =EdotCostPositiveWork;