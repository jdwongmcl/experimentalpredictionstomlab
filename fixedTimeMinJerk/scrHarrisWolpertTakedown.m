
PARAMS = getVUArmSegmentParameters();

fsim = {@objMinVarianceTorques,@objMinVarianceFirstOrderMusAndPostHold,@objMinVarianceSecondOrderMusAndPostHold};

xybeg = [0.0,.25];
xyend = [0.0,0.34];
yrange=xyend(2)-xybeg(2);
tReach = .65; %hmm.
tHold = .5;
%% regenerate torque solution for hold
optfminsearch = optimset('fminsearch');
optfminsearch.TolX = 1e-3; %millimeter change to the input X positions.
optfminsearch.TolFun = 1e-1; %TolFun unit CM. so 5e-1 is half cm.
x0 = [linspace(xybeg(:,1),xyend(:,1),5),linspace(xybeg(:,2),xyend(:,2),5)];
opt = fminsearch(@(x)objMinVarianceTorques(x,xybeg,xyend,tReach,0.005,PARAMS),x0,optfminsearch);
 fprintf('warning hold is 0.0');

%% minjerk
X2MJT=[0.035    0.210    0.500    0.790    0.965]'*yrange+xybeg(2);
 
X1ofT = ones(5,1)*xybeg(1);
optMinJ = [X1ofT;X2MJT];
%%

% no hold
% TorX2ofTNoHold = [0.0820    0.2540    0.4680    0.7020    0.9120]*yrange+xybeg(2);
% optTorHoldSol= [X1ofT;TorX2ofT'];
%   0.000117929619777
%    0.000045772109851
%    0.000083356986127
%    0.000109052004536
%   -0.000099487512500
%    0.255109590895036
%    0.270750444421684
%    0.294785008850693
%    0.319237733652311
%    0.334779862711123
TorX2ofT = [0.0601    0.2368 0.4909 0.7584 0.9424] * yrange + xybeg(2);
optTorHoldSol= [X1ofT;TorX2ofT'];

f1sol2ofT = [0.0466 0.221 0.506   0.787   0.967] * yrange + xybeg(2);
optF1Sol = [X1ofT; f1sol2ofT(:)];

optF2X2ofT = [    0.0289    0.2011    0.5000    0.7889    0.9656] * yrange + xybeg(2);
optF2Sol = [X1ofT;optF2X2ofT(:)];
%%
knotsAll = [optTorHoldSol,optF1Sol,optF2Sol];

tic;
for isim = 1%:length(fsim)
  f=fsim{isim};
  for jopt = 1:size(knotsAll,2)
    xk = knotsAll(:,jopt);
    [fval(isim,jopt),speeds(isim,jopt).val,u2costs(isim,jopt),nomErr(isim,jopt),cartesianError(isim,jopt).val]=f(xk,xybeg,xyend,tReach,tHold,PARAMS);
  end
end
toc;
%%
% compare u2costs
close all;
names = ['tor';'mo1';'mo2']
figure;
for isim =1:length(fsim)
subplot(3,1,isim);
bar(fval(isim,:));
title(['simulating with ',names(isim,:)])
ylabel('average error (cm)');
set(gca,'xticklabels',{'soltor','solmo1','solmo2'});
xlabel('solutions ');
end
%%
figure;
for isim =1:length(fsim)
subplot(3,1,isim);
bar(u2costs(isim,:));
title(names(isim,:))
end

%%
figure;
for j=1:3
  plot(speeds(1,j).val.t,speeds(1,j).val.speed);hold on;
end
