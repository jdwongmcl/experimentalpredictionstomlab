
%% opt = fmincon(@(x)objMinVariance(x,xybeg,xyend,tdur),x0,[],[],[],[],...
tic;  
xybeg = [0.05,0.15];
xyend = [0.05,0.25];
tdur = 0.4;
tHoldDur = 0.4;
X2ofT=0.1+[0.0035    0.0210    0.0500    0.0790    0.0965]';
X1ofT = ones(5,1)*.05;
XH5ofT = [X1ofT;X2ofT];

[valMJ,velsMJ]=objMinVarianceFirstOrderMusAndPostHold(XH5ofT,xybeg,xyend,tdur);

optNoHold=  [   0.0496    0.0494    0.0500    0.0510    0.0511    0.1082    0.1254    0.1468    0.1702    0.1912];
[fvalOpt,velsOptNoHold]=objMinVarianceFirstOrderMusAndPostHold(optNoHOld,xybeg,xyend,tdur,.5);

%%

tdur = 0.4;

optHold = [0.050000958073003   0.049923683185615   0.050456937644185   0.050140135356893   0.049855933195412   0.104658120199595   0.122113438122159 0.150641270898226   0.178747655949315   0.196704142619806];
[val(5),vels(5)]=objMinVarianceFirstOrderMusAndPostHold(optHold,xybeg,xyend,tdur,.5);

%%

tdur = .4; %hmm. 
x0 = [linspace(xybeg(:,1),xyend(:,1),5),linspace(xybeg(:,2),xyend(:,2),5)];


optfminsearch = optimset('fminsearch');
optfminsearch.TolX = 1e-3;
optfminsearch.TolFun = 1e-1;

for i =[5]
    tic
    opt = fminsearch(@(x)objMinVarianceFirstOrderMusAndPostHold(x,xybeg,xyend,tdur,i/10),x0,optfminsearch);
    opts(i).knots = opt;
    [val(i),vels(i)]=objMinVarianceFirstOrderMusAndPostHold(opt,xybeg,xyend,tdur,i/10);
    durs(i)=toc;
end
%started at 1:30
%
%%tolx&tolfun=1e-2 opt=     0.0496    0.0494    0.0500    0.0510    0.0511    0.1082    0.1254    0.1468    0.1702    0.1912
[fval,vels]=objMinVarianceFirstOrderMusAndPostHold(opt,xybeg,xyend,tdur,i/10);

%%

for i =[1,2,3,4,5,10]
    h=plot(vels(i).t,vels(i).speed);hold on;
end

savefig(h,'handSpeedMinimumVariance');