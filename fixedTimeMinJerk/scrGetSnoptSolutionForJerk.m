
PARAMS = getVUArmSegmentParameters();
%PARAMS.l(1)=0.301;PARAMS.l(2) = 0.301; % this is a stubby arm. we need 0.6cm reach for Harris Wolpert.
PARAMS.l(1) = 0.35;
PARAMS.dampCoef = .2;
PARAMS.SDN = 0.05; % X*100=% noise; of course not used in the TL model.
%% arbitrary movement parameters to match Hogan et al. 1985 figure.
tgtHW=[0,.3;
    -.22,.31;
    -.2,.5;
    0,.6;
    .2,.35];

tgtlist = [1,3;
    1,4;
    5,4;
    5,2;
    5,3];
tdur = 0.65;

%% snopt solution of force rate. 
for i =1%1:length(tgtlist)
    xybeg = tgtHW(tgtlist(i,1),:);
    xyend = tgtHW(tgtlist(i,2),:);
    nameLabel=[num2str(tgtlist(i,1)),num2str(tgtlist(i,2)),'normv'];
    coefWongCluffKuo = 8.5e-2;
    PARAMS.Objective = 'torqueRate';
    output = snoptDoublePendulumForceRateAndWorkFixedTime(xybeg,xyend,tdur,PARAMS,coefWongCluffKuo,nameLabel);
    figure(1001);hold on;
    plot(output.hand.t,output.hand.speed);hold on; 
    
end

%% compare to other costs
PARAMS.Objective = 'torqueRate';
tgtPair = tgtlist(2,:);
xybeg = tgtHW(tgtPair(1),:);
xyend = tgtHW(tgtPair(2),:);
%%
outputTorqueRate = snoptDoublePendulumForceRateAndWorkFixedTime(xybeg,xyend,tdur,PARAMS,coefWongCluffKuo,nameLabel);

%PARAMS.doJerk = 1;
%outputJerk = snoptDoublePendulumForceRateAndWorkFixedTime(xybeg,xyend,tdur,PARAMS,0);