% scrPointMass
% Goal: Simplest optimal control model. 

nameSystem = 'pointMass'; %this is not programmatic. verify that your label matches what you're doing!
x_start = [0;
           0];
x_targets = [0;
             0.1];

  for iTargets = 1:size(x_targets,2)

  nodes = 10:10:30;
  for iNodes = 1:length(nodes) %LOOP THROUGH 3 sets of nodes.
    if iNodes==1
      tic
    end
    disp(['Round: ',num2str(iNodes)])

    %%%setup the optimal control problem by defining the
    %%%1 SYSTEM TIME, STATES AND CONTROLS
    %%%2 EQUATIONS OF MOTION
    %%%3 TASK CONSTRAINTS
    %%%4 OBJECTIVE
    %%%5 INITIAL GUESSES FOR time, state, control

    
    %%% STEP 1: system time, states and controls.
    toms t % TIME VARIABLE
    tEnd = 1; % TIME VALUE: we're not going to optimize it/let it change.
    curNodes=nodes(iNodes);
    p = tomPhase('p', t, 0, tEnd, curNodes, [], 'spline3');
    setPhase(p);
    x = tomState('fi',2,1);             % 
    dxdt = tomState('dxdt',2,1);        % 
    force = tomState('force',2,1);      % 
    powerConstrainedPos = tomState('powerConstrainedPos',2,1);
    frrConstrainedPos = tomState('frrConstrainedPos',2,1);
    %%%/ end step 1. 
    
    %%% STEP 2: EQUATIONS OF MOTION 
    F=[0; 0]; %no external forces acting on hand
    
    m = 1; 
    MassMat = [m,0;0,1];

    cEquationsOfMotion = collocate({
      dot(x) == dxdt
      MassMat*dot(dxdt) == force + F;
      });
    
    %%% /END STEP 2. 
    
    %%% STEP 3: TASK CONSTRAINTS. 
    x_target = [x_targets(1,iTargets);x_targets(2,iTargets)];
   
  %%% Boundary constraints
    cBnd = {
      initial({
      x == x_start;
      dxdt == 0;      
      })
      final({x == x_target;
      dxdt == 0;     
      })
      };
    
    %%% Box constraints
    dxdtMax = 5;
    cBox = {
      collocate({dxdt <= dxdtMax
      -dxdtMax <= dxdt});
    };
	  
    cBndAccel = {initial(force) == final(force); ...
  	  		initial(dot(force)) == final(dot(force));
            
  	};
    cBndAccel = {};
	%%% /END STEP 3.
    
    %%% STEP 4: initial guess
    if iNodes==1

      %initial guess
      x0 = {
        %tEnd == 1 %if tEnd isn't fixed, tomlab DEMANDS we set it first.
        icollocate({
        x  == (x_start-x_target)/2;%vec(interp1([0 time],[fi_start; fi_target],t))'
        dxdt  == (x_target-x_start)/1;%jdw hack
        })
        collocate({
        })
        };
      %%% otherwise warmstart
    else
      x0 = {
        %tEnd == tEndopt %%as above for tEnd.
        icollocate({
        x == x_opt
        dxdt == dxdt_opt
        })
        
        collocate({
        force == force_opt
        })
        };
    end
    
    %%% STEP 4: Objective
    
    %%% Work cost
    powerX = force(1)*dxdt(1);
    powerY = force(2)*dxdt(2);
    kMechanicalWorkEfficiencyMargaria = 4.8; %imaginary scaling: muscles are only 25% efficient.
    mechPower = [powerX;powerY];
    
    cMPSlack = mcollocate({powerConstrainedPos >= mechPower
        powerConstrainedPos >=0
    });
    fraterate = dot(dot(force));
    cFRSlack = mcollocate({frrConstrainedPos >= fraterate
        frrConstrainedPos >=0
    });
        

    costPowerPositive = sum(integrate(powerConstrainedPos))*kMechanicalWorkEfficiencyMargaria/tEnd;
    
    %%% Force rate cost
    nnshift = 0.0000001;
    cForceRateRate = 8.5e-2;
    costForceRateQuad = cForceRateRate*sum(integrate((dot(dot(force'))).^2+nnshift));
    costForceRateLin = cForceRateRate*sum(integrate(frrConstrainedPos))/tEnd;
    costPowerForceRate = costForceRateLin;
    objective = (costPowerForceRate + costPowerPositive)*tEnd;
    
    nameObj='WorkFR';
    %%% /end step 4.
    
    %% Solve the problem
    options = struct;
    options.name = [nameSystem,' ',nameObj,'.m'];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    Prob = sym2prob(objective, {cBox, cBnd, cEquationsOfMotion, cBndAccel, cMPSlack, cFRSlack}, x0, options);
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    x_opt = subs(x, solution);
    dxdt_opt = subs(dxdt, solution);
    force_opt = subs(force, solution);
    
  end %end dampingLevel loop
  
  %%%add to solution vector
  solutions(iTargets) = solution;
  %%%get values
  
  EdotAvgPosPower(iTargets) = subs(costPowerPositive,solution)/kMechanicalWorkEfficiencyMargaria;
  EdotCostMechPower(iTargets) = subs(costPowerPositive,solution);
  EdotCostForceRate(iTargets) = subs(costPowerForceRate,solution);
  
  ECostForceRate(iTargets) = EdotCostForceRate(iTargets)*tEnd;
  ECostMechPower(iTargets) = EdotCostMechPower(iTargets)*tEnd;
  
  
  times(iTargets) = tEnd;
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('TARGET %i\n',iTargets);
  fprintf('Fdotdot Met power = %.4f \n',EdotCostForceRate(iTargets));
  fprintf('Mech Met Power = %.2f \n',EdotCostMechPower(iTargets));
  fprintf('Avg positive power = %.4f\n',EdotAvgPosPower(iTargets));
  
  s=[x(1) x(2) dxdt(1) dxdt(2)];
  ang = 0;%grav_angle*180/pi;
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  Exits(iTargets,:) = Exit;
  %%
  t_sol = linspace(0,subs(tEnd,solution),100)'; % Time vector where we want our trajectory evaluated.
  
  s_sol = atPoints(t_sol,subs(s,solution));
  force_sol = atPoints(t_sol,subs(force,solution));
  x_sol=s_sol(:,[1 2]);
  dxdt_sol=s_sol(:,[3 4]);
  
  figure(8); hold on;
  plot(t_sol,sqrt(dxdt_sol(:,1).^2+dxdt_sol(:,2).^2));
  ylabel('tan vel'); xlabel('Time (s)');
  figure(9); hold on;
  plot(t_sol,force_sol(:,1));
  plot(t_sol,force_sol(:,2));
  ylabel('Force (N)');xlabel('Time (s)');
  tor = atPoints(t_sol,subs(force,solution));
  t=t_sol;
  state = [x_sol dxdt_sol];
  figure(10); hold on;
  plot(t_sol,x_sol);
  ylabel('Position (m)');xlabel('Time (s)');
  end 

cd figures
eval(['save ',[nameSystem,nameObj],' Exits EdotAvgPosPower EdotCostMechPower EdotCostForceRate ECostMechPower t tEnd curNodes s x_sol dxdt_sol force_sol objective f_k Exit solution'])
toc
%%
Exits
figure(8);
savefig([nameSystem,nameObj,'_dqdt']);
figure(9);
savefig([nameSystem,nameObj,'_u']);
figure(10);
savefig([nameSystem,nameObj,'_x']);
figure(11);
savefig([nameSystem,nameObj,'_energybreakdown']);
cd ../
