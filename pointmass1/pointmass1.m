%%% simulations of 2018 reaching experiment investigating force-rate as a
%%% possible contributing effort cost during reaching. In the Introduction
%%% section of a paper and poster, it would be great to show what can and
%%% cannot be predicted by optimizations. in particular we could show:
% 1. jerk is equivalent to force-rate for getting smooth movements.
% 2. energy is impulsive
% 3. f^2 is something sensible.

warning off
% ca


load params_TOMLAB
OPT_TORQUE_CHANGE = 1;
OPT_TORQUE_SQUARED = 2;
OPT_ENERGY = 3;
OPT_TORQUE_ABS = 4;
OPT_FRATERATENOSTATE=5;

OPT_ENUM = 3;
nameOpts = {'TorqueChange','TorqueSquared','PositiveWork','TorqueAbs','RateRateNoState'};


l=[.315 .421];
% fi_targets=cart2polar([-.18 .22; -.18 .52],l); % calculate start and target angles
fi_targets = [62.0000   87.0000
  66.3376   82.6624
  68.6401   80.3599
  70.0806   78.9194
  70.9822   78.0178]*pi/180;

durs = [0.8571    0.6451    0.5173    0.4286    0.3681];
nn =        [10 20 30 40 50 60 70 80 160]; %twice 20 is because the 2nd 20 calculation inludes q=f(gamma)

for iCurrentObj = OPT_FRATERATENOSTATE
  
  for TGT = 1
    
    fi_start  = [fi_targets(TGT,1),fi_targets(TGT,1)+5/180*pi];
    fi_target = [fi_targets(TGT,2),fi_targets(TGT,2)+5/180*pi];    
    time = durs(TGT);
    
    %overwrite
    fi_start = [0,0];
    fi_target = [1,0];    
    time = 1;
    
    %check on start position of hand
    hand_start=[sum(l.*cos(fi_start)) sum(l.*sin(fi_start))];
    hand_target=[sum(l.*cos(fi_target)) sum(l.*sin(fi_target))];
    %% calculating initial states etc...
    F=[0 0]; %no external forces acting on hand
    %[stim_start, q_start, gamma_start, lce_start, Fdes, Ttot] = calc_stable_start2(fi_start,F,grav_angle,l);
    stim_start=[0,0];
    disp('calculated equilibrium start... now running OC problem')
    for iNN = 1:length(nn)
      if iNN==1
        tic
      end
      %     load params_TOMLAB
      
      l=[.315 .421];   % overrule segment length, now includes ~length of forearm plus hand
      
      steps=nn(iNN);
      toms t
      
      p = tomPhase('p', t, 0, time, steps, [], 'spline3');
      setPhase(p);
      
      fi = tomState('fi',1,2);        % segment angles
      fid = tomState('fid',1,2);      % segment angular velocities
      stim = tomControl('stim',1,2);  % jer: torque
      %%% initial guess
      if iNN<2
        x0 = {
          icollocate({
          fi  == vec(interp1([0 time],[fi_start; fi_target],t))'
          fid  == (fi_target-fi_start)/time
          })
          collocate({
          %stim  == stim_start %relative muscle activation
          %         stim   == [vec(interp1([0 time],[4.5; 4.51],t/2))',vec(interp1([0 time],[-4.5; -4.51],t/2+1))']
          })
          };
        %%% otherwise warmstart
      else
        x0 = {icollocate({
          fi == fiopt
          fid == fidopt
          })
          
          collocate({
          stim == stimopt
          })
          };
      end
      
      %%% Boundary constraints
      cbnd = {
        initial({
        fi == fi_start;
        fid == 0;
        stim == stim_start;
        })
        final({fi == fi_target;
        fid == 0;
        dot(fid)==0;
        stim == stim_start;
        })
        };
      
      %%% Box constraints
      cbox = {
        %       mcollocate(0 <= fi(1) <= 3*pi/2)
        %       mcollocate(-pi/4 <= fi(2)-fi(1) <= pi)
        %mcollocate(-100 <= fid <= 100)
        
        %0.005 <=  collocate(stim)  <= 1
        
        };
      
      disp(['Round: ',num2str(iNN)])
      
      %%%
      % ODEs and path constraints
      % Skeletal Dynamics
      % dynamics of double pendulum, including gravity and
      % external forces on the hand/wrist (e.g. simulated robotic forces).
      % avoiding (slow) matrix calculations.
      mb = 0; %mass of robotic manipulandum
      g=0;%*sin(grav_angle); % gravity w.r.t. angle of chair
      Fx=0;Fy=0; % no external forces on the hand.
      
      
      % Using Lagrangian Dynamics
      %     a_b=fi(1)-fi(2);
      %     a1=(m(1)*d(1)*d(1) + m(2) * l(1)*l(1)) + I(1) + mb*l(1)^2;
      %     a2=m(2)*l(1)*d(2)*cos(a_b) + mb*l(1)*l(2)*cos(a_b);
      %     a3=m(2)*l(1)*d(2)*cos(a_b) + mb*l(1)*l(2)*cos(a_b);
      %     a4=m(2)*d(2)*d(2) + mb*l(2)*l(2) +I(2);
      %
      %     b1=(-1) * m(1)*g*d(1)*cos(fi(1)) - m(2)*g*l(1)*cos(fi(1)) + Fy*l(1)*cos(fi(1)) - Fx*l(1)*sin(fi(1)) - m(2)*l(1)*d(2)*sin(a_b)*fid(2)*fid(2) - mb*l(1)*l(2)*sin(a_b)*fid(2)*fid(2) - mb*g*l(1)*cos(fi(1)) + stim(1) - stim(2);
      %     b2=(-1) * m(2)*g*d(2)*cos(fi(2))                          + Fy*l(2)*cos(fi(2)) - Fx*l(2)*sin(fi(2)) + m(2)*l(1)*d(2)*sin(a_b)*fid(1)*fid(1) + mb*l(1)*l(2)*fid(1)*fid(1)*sin(a_b) - mb*g*l(2)*cos(fi(2)) + stim(2);
      %     %
      % Equality contraints
      ceq = collocate({
        dot(fi') == fid'
        dot(fid') == stim'./[m(1);m(1)];
        });
      
      %%% Objectives
      %     Pmus = gse.*vcerel.*rlceopt;
      k = [1 1 1 2 3 3.5 4 4.5];
      %         objective = k(i)*(integrate(sum(stim.^2)+sum(.002*dot(stim).^2)))+alpha^2;
      
      %objective = integrate(sum(stim.^2));
      %objective = sum(integrate(dot([a1 a2; a3 a4]*dot(fid')).^2));
      
      nameOpt = nameOpts{iCurrentObj};
      
      % smoothing terms worked but not great.
      % am cheating big time by adding smoothing terms. 
      % am getting 'cannot be improved' errors 
      % massive scaling problem for torque2 and energy
      % evidence: exit=10 inform=41
      % https://tomopt.com/docs/TOMLAB_SNOPT.pdf
      % suggests: bad scaling
      % suggests: 
      % dinant thinks scaling should be between 0.1 and 100.
      % 2018-10-25: this would mean
      % opt_energy should be divided by 1000. 
      smoothingTerm= 0;
      switch iCurrentObj
        case OPT_TORQUE_CHANGE
          objective = sum(integrate(dot(stim').^2));
          
        case OPT_TORQUE_SQUARED
          objective = sum(integrate((stim').^2))  ;
          smoothingTerm = sum(integrate(2e-5*dot(stim').^2));
        case OPT_ENERGY
          thePower = (stim').*fid';
          objective = sum(integrate(0.0001*(thePower+abs(thePower))/2));
          smoothingTerm = 0;
          %smoothingTerm = sum(integrate((2e-12)*dot(stim').^2));
          
        case OPT_TORQUE_ABS
          objective = sum(integrate(0.0001*(sqrt((stim').^2+0.00001))))/1e2;
        case OPT_FRATERATENOSTATE
          objective = sum(integrate((0.00001*dot(dot(stim'))).^2));
          
      end
      if iNN<(length(nn)+1)
        objective = objective + smoothingTerm;
      end
      nameSim='pointmass';
      %% Solve the problem
      options = struct;
      options.name = [nameSim,' ',nameOpt,'.m'];
      options.scale ='auto';
      options.PriLev = 2;
      options.Prob.SOL.optPar(35) = 100000; %iterations limit
      options.Prob.SOL.optPar(30) = 200000; %major iterations limit
      Prob = sym2prob(objective, {cbox, cbnd, ceq}, x0, options);
      result = tomRun('snopt',Prob,1);
      solution = getSolution(result);
      
      fiopt = subs(fi, solution);
      fidopt = subs(fid, solution);
      
      stimopt = subs(stim, solution);
      
      F = subs([Fx Fy],solution);
      s=[fi(1) fid(1) fi(2) fid(2)];
      
    end
    ang = 0;%grav_angle*180/pi;
    f_k=result.f_k;
    Exit=[result.ExitFlag result.Inform];
    cd optim_data
    eval(['save ',nameOpt,nameSim,num2str(ang),'_',num2str(TGT),' stim t time steps s fi fid objective f_k Exit solution'])
    cd ..
    
    
    
    
    %%
    t_plot = linspace(0,time,451)'; % Time vector where we want our trajectory evaluated.
    u_plot = collocate(t);
    % figure(1);
    s_plot = atPoints(t_plot,subs(s,solution));
    stim_u = atPoints(u_plot,subs(stim,solution));
    fi=s_plot(:,[1 3]);
    fip=s_plot(:,[2 4]);
    % [e,h]=showmov3(fi,l,s_plot,0);
    % plotvs(h)
    % axis equal
    figure(8); hold on;
    plot(t_plot,fip(:,1),'color',matlabColor(iCurrentObj));
    ylabel('Velocity (m/s)'); xlabel('Time (s)');
    figure(9); hold on;
    plot(u_plot,stim_u(:,1),'color',matlabColor(iCurrentObj));
    ylabel('Force (N)');xlabel('Time (s)');
    tor = atPoints(t_plot,subs(stim,solution));
    t=t_plot;
    state = [fi fip];
    figure(10); hold on;
    plot(t_plot,fi,'color',matlabColor(iCurrentObj));
    xlabel('Position (m)');xlabel('Time (s)');
  end
end
%%
cd answerfigures;
figure(8);
savefig([nameOpt,nameSim,'_dxdt']);
figure(9);
savefig([nameOpt,nameSim,'_u']);
figure(10);
savefig([nameOpt,nameSim,'_x']);
cd ../