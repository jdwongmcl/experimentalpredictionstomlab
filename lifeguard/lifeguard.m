% replicate DAK 2014 stuff using EOM with arm only...
% 
% [acceleration constraint]; but this isn't correct since movements were not minjerk)
clear all;

% we set the coefficient to force rate in an if statement.
cReward = 1;
cRestingRate = 1;
cObj=.0001;

DISCRETE1CONTINUOUS2 = 1;
ABS1QUAD2 = 2;

nameObjective = 'workfr';
nameTask = 'lifeguard';

m3 = 0;
nameSys=['armEndpointMass',num2str(m3)','kg'];
labelThisSim = ['OBJ',nameObjective,'TSK',nameTask,'SYS',nameSys] %this is not programmatic. verify that your label matches what you're doing!
fprintf('press enter to optimize:\n');
pause

xy_start = [.30,.20];
xy_target = [0,0.5];
%% compute the targets
% FROM JDW CLUFF KUO, the target locations and durations.
P = load('paramsTomlabJDW.mat');
l1 = P.l(1);
l2 = P.l(2);

x=xy_start(1);y=xy_start(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);

q_start = [q1,q2];


x=xy_target(1);y=xy_target(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);

q_target = [q1,q2];

sTimeMovement =0.5;

% LOOP THROUGH ALL viscosities
viscositiesWater = [0];
for iVisc = 1:length(viscositiesWater) %1:length(fi_targets_sho)
  iTGT = 1;%we only have one target.
  fi_start  = [q_start(iTGT,:)];
  fi_target = [q_target(iTGT,:)];
  
  
  %% calculating initial states etc...
  disp('calculated equilibrium start... now running OC problem')
  gradualSteps = linspace(10,20,2);
  for iSteps = 1:length(gradualSteps) %LOOP THROUGH 3 DAMPING LEVELS, which seems to help.
    if iSteps==1
      tic
    end
    disp(['Round: ',num2str(iSteps)])
    
    %%%setup the tomlab problem by defining the
    %%%1 TIME
    %%%2 PHASE
    %%%3 CONTROLS
    %%%4 STATES
    toms t %
    tHalf = sTimeMovement(iTGT);
    tend = tHalf;
    
    p = tomPhase('p', t, 0, tHalf, gradualSteps(iSteps), [], 'spline3');
    setPhase(p);
    fi = tomState('fi',1,2);        % segment angles
    fid = tomState('fid',1,2);      % segment angular velocities
    stim = tomState('stim',1,2);  % jer: torque
    %%%/setup the tomlab problem by defining the
    
    %%% initial guess
    if iSteps==1
      %initial guess
      x0 = {
        %tend == 1 % WARNING: if T IS AN OPT VARIABLE, then THIS NEEDS TO BE FIRST IN LIST. propt documentation.
        icollocate({
        fi  == (fi_start-fi_target)/2;%vec(interp1([0 time],[fi_start; fi_target],t))'
        fid  == (fi_target-fi_start)/1;%jdw hack
        })
        collocate({
        })
        };
      
      %%% otherwise warmstart
    else
      x0 = {
        %tend == tendopt %%here we are not solving for this!!
        icollocate({
        fi == fiopt
        fid == fidopt
        })
        
        collocate({
        stim == stimopt
        })
        };
    end
    
    switch DISCRETE1CONTINUOUS2
      case 1
        DiscOrCont = 'Discrete';
        %these 1/10 are attempting to get the accels and velocities and
        %positions similar scaling.
        %       cdiscretecontinuous = { ...
        %         1/10*initial(dot(fid))' == 0
        %         1/10*atPoints(tHalf,dot(fid))' == 0; ...
        %         1/10*initial(dot(fid))' == 1/10*final(dot(fid))'};
        
        cdiscretecontinuous = { ...
          initial(stim)' == 0
          final(stim)' == 0};
        cwrap={
          };
        
        
      case 2
        DiscOrCont = 'Continuous';
        cdiscretecontinuous = {};
        cwrap = {1/10*initial(stim)'==1/10*final(stim)'; ...
          1/100*initial(dot(stim))' == 1/100*final(dot(stim))'
          };
    end
    
    switch ABS1QUAD2
      case 1
        cForceRateRate = 4e-2;   %roughly matching wong cluff kuo 2020
      case 2
        cForceRateRate = 1.2e-4; %roughly matching wong cluff kuo 2020
    end
    
    
    
    %%% Boundary constraints
    cbnd = {
      initial({
      fi == fi_start;
      fid == 0;
      })
      final({fi == fi_target;
      fid == 0;
      })
      };
    
    %%% Box constraints
    
    %%%
    % ODEs and path constraints via virtual power
    
    P = load('paramsTomlabJDW.mat');
    m = P.m;m1 = m(1);m2=m(2);
    I = P.I;I1 = I(1);I2 = I(2);
    r = P.l/2;
    l = P.l;
    l1 = l(1);l2 = l(2);
    d = l - r;d1 = d(1);d2 = d(2);
    
    % added forces at the hand
    b = 225;
    Mjac = [ -l1*sin(fi(1)), -l2*sin(fi(2));
      l1*cos(fi(1)),  l2*cos(fi(2))];
    
    Xhand = [l1*cos(fi(1))+l2*cos(fi(2));
              l1*sin(fi(1))+l2*sin(fi(2))];

    
    Vhand = Mjac*fid';
    %Fhand = [0;b*(-Vhand(2)+Vhand(1))*sqrt((xy_target(2)-Xhand(2))^2)];
    Xshore = 0.15;
    viscositySand = 0;
    viscosityWater = viscositiesWater(iVisc);
    Fhand = ifThenElse(Xhand<Xshore, [-1;-1].*Vhand.*viscosityWater,0);
    Fhand = [0;0];
    %Fy = ifThenElse(Xhand<Xshore, -1*Vhand(2).*viscosityWater,0);
    %Fx = Fhand(1);Fy = Fhand(2);
    %Fhand = [Fx,Fy];
    
    tauEquiv = Mjac'*Fhand;
    mb = 0;%simulated mass
    g = 0;
    
    MassMat = [ I1 + d1^2*m1 + l1^2*m2 + l1^2*m3, l1*cos(fi(1) - fi(2))*(d2*m2 + l2*m3);
      l1*cos(fi(1) - fi(2))*(d2*m2 + l2*m3),          m2*d2^2 + m3*l2^2 + I2];

     F = [-l1*fid(2)^2*sin(fi(1) - fi(2))*(d2*m2 + l2*m3)
      l1*fid(1)^2*sin(fi(1) - fi(2))*(d2*m2 + l2*m3)];
  
 
    ceq = collocate({dot(fi') == fid'
          1/10*(MassMat*dot(fid')) == 1/10*(F + [tauEquiv(1)-tauEquiv(2);tauEquiv(2)] + [stim(1)-stim(2);stim(2)]);
    });
%     a_b=fi(1)-fi(2);
%     a1=(m(1)*d(1)*d(1) + m(2) * l(1)*l(1)) + I(1) + mb*l(1)^2;
%     a2=m(2)*l(1)*d(2)*cos(a_b) + mb*l(1)*l(2)*cos(a_b);
%     a3=m(2)*l(1)*d(2)*cos(a_b) + mb*l(1)*l(2)*cos(a_b);
%     a4=m(2)*d(2)*d(2) + mb*l(2)*l(2) +I(2);
%     
%     b1=(-1) * m(1)*g*d(1)*cos(fi(1)) - m(2)*g*l(1)*cos(fi(1)) + Fy*l(1)*cos(fi(1)) - Fx*l(1)*sin(fi(1)) - m(2)*l(1)*d(2)*sin(a_b)*fid(2)*fid(2) - mb*l(1)*l(2)*sin(a_b)*fid(2)*fid(2) - mb*g*l(1)*cos(fi(1)) + stim(1) - stim(2);
%     b2=(-1) * m(2)*g*d(2)*cos(fi(2))                          + Fy*l(2)*cos(fi(2)) - Fx*l(2)*sin(fi(2)) + m(2)*l(1)*d(2)*sin(a_b)*fid(1)*fid(1) + mb*l(1)*l(2)*fid(1)*fid(1)*sin(a_b) - mb*g*l(2)*cos(fi(2)) + stim(2);
    
%     ceq = collocate({
%     dot(fi') == fid'
%     [a1 a2; a3 a4]*dot(fid') == [b1; b2]
% });
    
    %%% Objective
    
    
    %%% Work cost
    powerSho = stim(1)*fid(1) - stim(2)*fid(1);
    powerElb = stim(2)*fid(2);
    posPowerSho = integrate((powerSho+abs(powerSho))/2);
    posPowerElb = integrate((powerElb+abs(powerElb))/2);
    costPositiveWork = posPowerSho + posPowerElb;
    kMechanicalWorkEfficiencyMargaria = 4.2;
    costPositiveWork = costPositiveWork*kMechanicalWorkEfficiencyMargaria;
    
    %%% Force rate cost
    nnshift = 0.00001;
    costForceRateLin = cForceRateRate*sum(integrate(sqrt((dot(dot(stim'))).^2+nnshift)));
    costForceRateQuad = cForceRateRate*sum(integrate((dot(dot(stim'))).^2+nnshift));
    costForceRate = costForceRateLin;
    
    %%% Resting cost
    restingMetRate = 100;%100 Watts.
    costResting = sqrt(tend^2+0.00001)*restingMetRate;% approximate linear in case it's a problem.
    nameOpt = [labelThisSim,DiscOrCont];
    
    
    %%% Reward cost
    reward = 0;%cReward * sqrt(tend^2+0.00001); %avoiding tend<0?
    
    switch nameObjective
      case 'u2'
        objective = sum(integrate(stim.^2));
      case 'workfr' 
        objective = cObj * (costForceRate + costPositiveWork);
    end
    
    
    %% Solve the problem
    options = struct;
    options.name = [nameSys,' ',nameOpt,'.m'];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    Prob = sym2prob(objective, {cbnd, ceq, cwrap, cdiscretecontinuous}, x0, options);
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    fiopt = subs(fi, solution);
    fidopt = subs(fid, solution);
    stimopt = subs(stim, solution);
    
  end %end dampingLevel loop
  
  %%%add to solution vector
  solutions(iVisc) = solution;
  %%%get values
  ECostForceRate(iVisc) = subs(costForceRate,solution);
  ECostPositiveWork(iVisc) = subs(costPositiveWork,solution);
  ECostResting(iVisc) = subs(costResting,solution);
  EReward(iVisc) = subs(reward,solution);
  
  EdotCostForceRate(iVisc) = subs(costForceRate,solution)./tend;
  EdotCostPositiveWork(iVisc) = subs(costPositiveWork,solution)./tend;
  EdotCostResting(iVisc) = subs(costResting,solution)./tend;
  EdotReward(iVisc) = subs(reward,solution)./tend;
  times(iVisc) = tend;
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('Viscosity: %i\n',viscositiesWater(iVisc));
  fprintf('Fdotdot power = %.4f \n',EdotCostForceRate(iVisc));
  fprintf('Avg positive power = %.4f\n',EdotCostPositiveWork(iVisc));
  fprintf('Resting power = %.4f\n',EdotCostResting(iVisc));
  fprintf('Reward Power = %.4f\n',EdotReward(iVisc));
  
  s=[fi(1) fi(2) fid(1) fid(2)];
  ang = 0;%grav_angle*180/pi;
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  Exits(iVisc,:) = Exit;
  %%
  t_plot = linspace(0,subs(tend,solution),1000)'; % Time vector where we want our trajectory evaluated.
  %u_plot = collocate(t);
  % figure(1);
  s_plot = atPoints(t_plot,subs(s,solution));
  stim_u = atPoints(t_plot,subs(stim,solution));
  fiplot=s_plot(:,[1 2]);
  fidplot=s_plot(:,[3 4]);
  
  [xyhand,fhand,vxyhand] = deal(zeros(size(fiplot,1),size(fiplot,2)));
  
  for iloop =1:length(fiplot)
    qt = fiplot(iloop,:)';
    dqdt = fidplot(iloop,:)';
    Mjac = [ -l1*sin(qt(1)), -l2*sin(qt(2));
      l1*cos(qt(1)),  l2*cos(qt(2))];
    
    vxy = Mjac*dqdt;
    vxyhand(iloop,:) = vxy';
    xyhand(iloop,:) = [l1*cos(qt(1))+l2*cos(qt(2));
      l1*sin(qt(1))+l2*sin(qt(2))];
    fhand(iloop,:) = [0;b*(-vxy(2)+vxy(1))*sqrt((xy_target(2)-xyhand(iloop,2))^2)];
    
    jac1 = [ -d1*sin(qt(1)), 0;
              d1*cos(qt(1)), 0];
    jac2 = [ -l1*sin(qt(1)), -d2*sin(qt(2));
                l1*cos(qt(1)),  d2*cos(qt(2))];
    jac3 = [ -l1*sin(qt(1)), -l2*sin(qt(2));
              l1*cos(qt(1)),  l2*cos(qt(2))];
 
    DXY1 = jac1*dqdt;
    DXY2 = jac2*dqdt;
    DXY3 = jac3*dqdt;
    Ek(iloop,:) = 1/2 * m1 * (DXY1' * DXY1) + ...
                  1/2 * m2 * (DXY2' * DXY2) + ...
                  1/2 * m3 * (DXY3' * DXY3) + ...
                  1/2 * I1 * dqdt(1)^2 + ...
                  1/2 * I2 * dqdt(2)^2;
              
    
    PowEP(iloop,:) = sum(fhand(iloop,:).*vxyhand(iloop,:));
    PowJoints(iloop,:) = sum(stim_u(iloop,:)'.*dqdt);% - stim_u(iloop,2)*dqdt(1);
  end
  WorkEP = cumtrapz(t_plot,PowEP);
  WorkJoints = cumtrapz(t_plot,PowJoints);
  % [e,h]=showmov3(fi,l,s_plot,0);
  % plotvs(h)
  % axis equal
  figure(8); hold on;
  c = get(0, 'DefaultAxesColorOrder');
  plot(t_plot,sqrt(vxyhand(:,1).^2+vxyhand(:,2).^2),'color',c(iVisc,:));
  ylabel('Hand speed (m \cdot s^{-1})'); xlabel('Time (s)');
  figure(9); hold on;
  plot(t_plot,stim_u(:,1),'color',c(iVisc,:));
  plot(t_plot,stim_u(:,2),'color',c(iVisc,:));
  ylabel('Torque (N \cdot m)');xlabel('Time (s)');
  tor = atPoints(t_plot,subs(stim,solution));
  t=t_plot;
  state = [fiplot fidplot];
  figure(10); hold on;
  plot(t_plot,fiplot,'color',c(iVisc,:));
  xlabel('Position (m)');xlabel('Time (s)');
  
end
%% plot forcerate+work
figure(11);
freqs = 1./times;
plot(viscositiesWater,EdotCostPositiveWork,'marker','.','markersize',20);hold on;
plot(viscositiesWater,EdotCostForceRate,'marker','.','markersize',20);
plot(viscositiesWater,EdotCostForceRate+EdotCostPositiveWork,'marker','.','markersize',20);
legend({'work','frate','net'}, 'location','northwest');

ylabel('Power (W)');
xlabel('Frequency (Hz)');

cd figures;
eval(['save ',nameOpt,nameSys,' Exits ECostResting ECostPositiveWork ECostForceRate EReward EdotCostResting EdotCostPositiveWork EdotCostForceRate EdotReward times stim t tend gradualSteps s fi fid objective f_k Exit solution'])


toc

%% plot handpath
figure(12);
plot(xyhand(:,1),xyhand(:,2),'linewidth',2);hold on;
rs = 1:10:length(xyhand);
quiver(xyhand(rs,1),xyhand(rs,2),vxyhand(rs,1),vxyhand(rs,2));
quiver(xyhand(rs,1),xyhand(rs,2),fhand(rs,1),fhand(rs,2));
xlabel('x (m)');ylabel('y (m)');
legend('hand','speed','force');
axis equal

%% figure 13 energy bbalance
figure(13);
plot(t_plot,WorkEP,'linewidth',2);hold on;
plot(t_plot,WorkJoints,'linewidth',2);
plot(t_plot,Ek,'linewidth',4);
plot(t_plot,WorkEP+WorkJoints,'linewidth',2);
legend({'endpoint','joint','ekin','sumExternalWork'},'location','northwest');
%%
Exits
figure(8);
savefig([nameOpt,nameSys,'_dqdt']);
figure(9);
savefig([nameOpt,nameSys,'_u']);
figure(10);
savefig([nameOpt,nameSys,'_x']);
figure(11);
savefig([nameOpt,nameSys,'_energybreakdown']);
figure(12);
savefig([nameOpt,nameSys,'_quiver']);
figure(13);
savefig([nameOpt,nameSys,'_ebalance']);

cd ../
