% replicate DAK 2014 stuff using EOM with arm only...
% 
% [acceleration constraint]; but this isn't correct since movements were not minjerk)
clear all;

% we set the coefficient to force rate in an if statement.
cReward = 1;
cRestingRate = 1;
cObj=.0001;

DISCRETE1CONTINUOUS2 = 1;
ABS1QUAD2 = 2;

nameObjective = 'workfr';
nameTask = 'lifeguard';

nameSys=['armEndpointMass1kg'];
labelThisSim = ['OBJ',nameObjective,'TSK',nameTask,'SYS',nameSys] %this is not programmatic. verify that your label matches what you're doing!

xy_start = [.30,.20];
xy_target = [0,0.5];
ttotal = .5;
%% compute the targets
% FROM JDW CLUFF KUO, the target locations and durations.

% LOOP THROUGH ALL viscosities
viscositiesWater = [0,50];
for iVisc = 1:length(viscositiesWater) %1:length(fi_targets_sho)
  iTGT = 1;%we only have one target.
  
  %% calculating initial states etc...
  disp('calculated equilibrium start... now running OC problem')
  %gradualSteps = linspace(10,20,2);
  gradualSteps = 10;
  for iSteps = 1:length(gradualSteps) 
    if iSteps==1
      tic
    end
    disp(['Round: ',num2str(iSteps)])
    
    %%%setup the tomlab problem by defining the
    %%%1 TIME
    %%%2 PHASE
    %%%3 CONTROLS
    %%%4 STATES
    toms t t1dur t2dur yMid%   
    p = tomPhase('p', t, 0, t1dur, gradualSteps(iSteps), [], 'spline3');
    setPhase(p);
    xy = tomState('xy',1,2);        % segment angles
    dxydt = tomState('dxydt',1,2);      % segment angular velocities
    stim = tomState('stim',1,2);  % jer: torque
    frrConEq = tomState('frrConEq',1,2);
    mechPowerConEq = tomState('mechPowerConEq',1,2);
    %%%/setup the tomlab problem by defining the
    
    %%% initial guess
 
   switch ABS1QUAD2
      case 1
        cForceRateRate = 4e-2;   %roughly matching wong cluff kuo 2020
      case 2
        cForceRateRate = 1.2e-2; %roughly matching wong cluff kuo 2020
    end
        
    %%%
    % ODEs and path constraints via virtual power  
    P = load('paramsTomlabJDW.mat');
    m = P.m;m1 = m(1);m2=m(2);
    I = P.I;I1 = I(1);I2 = I(2);
    r = P.l/2;
    l = P.l;
    l1 = l(1);l2 = l(2);
    d = l - r;d1 = d(1);d2 = d(2);
    
    % added forces at the hand
    
    m1 = 1;
    MassMat = [m1,0;0,m1];
    F = [0;0];
 
    ceq1 = collocate(p,{dot(p, xy') == dxydt'
          (MassMat*dot(p,dxydt')) == (F + stim');
    });
    %%% Objective
    
    %%% Work cost
    powerSho = stim(1)*dxydt(1);
    powerElb = stim(2)*dxydt(2);
    mechPower = [powerSho,powerElb];
    cMPSlack = mcollocate(p,{mechPowerConEq >= mechPower
        mechPowerConEq >=0
    });
        
    kMechanicalWorkEfficiencyMargaria = 4.8;
    costPositivePower = sum(integrate(p,mechPowerConEq))*kMechanicalWorkEfficiencyMargaria/t1dur;
    
    %%% Force rate cost
    nnshift = 0.00001;
    
    fraterate = dot(p,dot(p,stim));
    cFRRSlack = mcollocate(p,{frrConEq >= fraterate
        frrConEq >= 0});

    costForceRateLin = cForceRateRate*sum(integrate(p,frrConEq))/t1dur;
    costForceRateQuad = cForceRateRate*sum(integrate(p,(dot(p,dot(p,stim'))).^2+nnshift));
    costForceRate = costForceRateQuad;
    
    %%% Resting cost
    restingMetRate = 100;%100 Watts.
    costResting = sqrt((t2dur+t1dur)^2+0.00001)*restingMetRate;% approximate linear in case it's a problem.
    nameOpt = [labelThisSim];
    
    if iSteps ==1
        x0 = {
        t1dur == 0.2
        t2dur == 0.2
        
        icollocate(p,{
        xy  == (xy_start-xy_target)/2;%vec(interp1([0 time],[fi_start; fi_target],t))'
        dxydt  == (xy_target-xy_start)/1;%jdw hack
        })
        };
    else
        x0 = {
            t1dur == t1duropt
            t2dur == t2duropt
    icollocate(p,{
        xy == xyopt
        dxydt == dxydtopt
        })
        collocate(p,{
        stim == stimopt
        })
    };
    end
    %%% Reward cost
    reward = 0;%cReward * sqrt(tend^2+0.00001); %avoiding tend<0?
    
    switch nameObjective
      case 'u2'
        objective1 = sum(integrate(p,stim.^2));
      case 'workfr' 
        objective1 = (costForceRate + costPositivePower)*t1dur;
    end
    
    
    %%%% now phase 2
    p2 = tomPhase('p2', t, t1dur, t2dur, gradualSteps(iSteps), [], 'spline3');
    setPhase(p2);
    xy_p2 = tomState('xy_p2',1,2);        % segment angles
    dxydt_p2 = tomState('dxydt_p2',1,2);      % segment angular velocities
    stim_p2 = tomState('stim_p2',1,2);  % jer: torque
    frrConEq_p2 = tomState('frrConEq_p2',1,2);
    mechPowerConEq_p2 = tomState('mechPowerConEq_p2',1,2);
    %%%/set up the tomlab problem by defining variables

    %%% EOM
    m1 = 1;
    MassMat = [m1,0;0,m1];
    F = [0;0];
    Fhand2 = [1;1]*-viscositiesWater(iVisc);
    
    ceq2 = collocate(p2,{dot(p2,xy_p2') == dxydt_p2'
          (MassMat*dot(p2,dxydt_p2')) == (F + stim_p2')+Fhand2;
    });
    %%% Objective
    
    %%% Work cost
    powerSho_p2 = stim_p2(1)*dxydt_p2(1);
    powerElb_p2 = stim_p2(2)*dxydt_p2(2);
    mechPower_p2 = [powerSho_p2,powerElb_p2];
    cMPSlack_p2 = mcollocate(p2,{mechPowerConEq_p2 >= mechPower_p2
        mechPowerConEq_p2 >=0
    });
        
    kMechanicalWorkEfficiencyMargaria = 4.8;
    costPositivePower_p2 = sum(integrate(p2,mechPowerConEq_p2))*kMechanicalWorkEfficiencyMargaria/t2dur;
    
    %%% Force rate cost
    nnshift = 0.00001;
    
    fraterate_p2 = dot(p2,dot(p2,stim_p2));
    cFRRSlack_p2 = mcollocate(p2,{frrConEq_p2 >= fraterate_p2
        frrConEq_p2 >= 0});

    costForceRateLin_p2 = cForceRateRate*sum(integrate(p2,frrConEq_p2))/t2dur;
    costForceRateQuad_p2 = cForceRateRate*sum(integrate(p2,(dot(p2,dot(p2,stim'))).^2+nnshift));
    costForceRate_p2 = costForceRateLin_p2;
    
    %%% Resting cost
    restingMetRate = 100;%100 Watts.
    
    %%% Reward cost
    reward = 0;%cReward * sqrt(tend^2+0.00001); %avoiding tend<0?
    
    switch nameObjective
      case 'u2'
        objective2 = sum(integrate(p2,stim_p2.^2));
      case 'workfr' 
        objective2 = (costForceRate_p2 + costPositivePower_p2)*t2dur;
    end

    cbnd1 = {
      initial(p,{
      xy' == xy_start';
      dxydt' == [0;0]
      dot(p,dxydt)' == [0;0]
      })
      final(p,xy') == initial(p2,xy_p2')
      
    };

    cdiscretecontinuous1 = { ...
      initial(p,dxydt)' == [0;0]
    };

    cdiscretecontinuous2 = { ...
      final(p2,dot(p2,dxydt_p2)') == [0;0]};
 
    %%% Boundary constraints
    cbnd2 = {
      final(p2,xy_p2') == xy_target'
      final(p2,dxydt_p2') == [0;0]
      };

    ctime = {t1dur + t2dur == ttotal};
    link ={
        final(p,xy)' == initial(p2, xy_p2)'
        final(p, dxydt)' == initial(p2, dxydt_p2)'
        };

    %%% initial guess
    if iSteps==1
      %initial guess
      x0_p2 = {    
        icollocate(p2,{
        %xy_p2  == (XSHORE);%vec(interp1([0 time],[fi_start; fi_target],t))'
        dxydt_p2  == (xy_target-xy_start)/t2dur;%jdw hack
        })
        };
      
      %%% otherwise warmstart
    else
      
      x0_p2 = {
        
        icollocate(p2,{
        xy_p2 == xy_p2opt
        dxydt_p2 == dxydt_p2opt
        })
        collocate(p2,{
        stim_p2 == stim_p2opt
        })
        };
    end
    
    objective = objective1+objective2;
    
    %% Solve the problem
    options = struct;
    options.name = [nameSys,' ',nameOpt,'.m'];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    Prob = sym2prob(objective, {cbnd1, ceq1, cbnd2, ceq2, ctime, cMPSlack,cFRRSlack,...
        cMPSlack_p2,cFRRSlack_p2, cdiscretecontinuous1, cdiscretecontinuous2, link},...
        {x0,x0_p2}, options);
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    xyopt = subs(xy, solution);
    dxydtopt = subs(dxydt, solution);
    stimopt = subs(stim, solution);
    xy_p2opt = subs(xy_p2, solution);
    dxydt_p2opt = subs(dxydt_p2, solution);
    stim_p2opt = subs(stim_p2, solution);
    t1duropt = subs(t1dur,solution);
    t2duropt = subs(t2dur,solution);
    
  end %end iSteps
  
  t_plot = subs(collocate(p,t),solution);
  t_plot = [t_plot;subs(collocate(p2,t),solution)];
  xy_plot = subs(collocate(p,xy),solution);
  xy_plot = [xy_plot;subs(collocate(p2,xy_p2),solution)];
  xdot = subs(collocate(p,dxydt),solution);
  xdot = [xdot;subs(collocate(p2,dxydt_p2),solution)];
  
  figure;
  plot(xy_plot(:,1),xy_plot(:,2));
end