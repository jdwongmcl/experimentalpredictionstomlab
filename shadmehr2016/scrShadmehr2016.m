% scrPredictMovementDurationForDistance
% Goal:
%
clear all;warning off

shadmehr2016=[
.1, 0.3954319976883064, 174.92414756308992
.1, 0.46785301483336517, 109.1010884222693
.1, 0.6169572336736658, 47.02249084954735
.1, 0.9244305047197071, 30.654016567135443
.1, 1.196217010209979, 25.025284145636704
.2, 0.45317015026006546, 230.67568869196683
.2, 0.6156027258717008, 104.36332113272974
.2, 0.9122218743979963, 47.48603351955302
.2, 1.3201093238297048, 30.25067424388368
.2, 2.2415900115584666, 20.901560392987903
.3, 0.6491764592564052, 183.07527451358118
.3, 0.7333726642265456, 118.76926411096127
.3, 0.8766615295704102, 52.87396455403581
.3, 1.2846573396262762, 31.051338855711833
.3, 1.7866921113465613, 28.24600269697555];


%% coefficients for force rate, and UNO. 
cForceRateRate = 1.2e-4; % quadratic cost 1.2e-4;%
cUnoTop = 1e-2; % we should in principle perhaps make this fit about right for our experiment.
cReward = 25;%this is a linear time cost per second, in W. uno 15. wfr 25
cObj=.0001;

COST_FR_R = 'workfrreward';
COST_FR_ONLY = 'workfronly';
COST_UNO_R = 'uno_reward';
objectiveFunction = COST_FR_R;

nameSim=['Task_DistanceAndDuration__Min_',objectiveFunction];


% LOOP THROUGH lengths similar to Reppert et al. 2018. 
lengths = shadmehr2016(:,1);
for iLength = 9%:length(lengths)
    
    xybeg = [-0.15,.15];
    xyend = [-0.15,0.15+lengths(iLength)];
    %% compute the targets
    % FROM JDW CLUFF KUO, the target locations and durations.
    P = load('paramsTomlabJDW.mat');
    l1 = P.l(1);
    l2 = P.l(2);
    
    x=xybeg(1);y=xybeg(2);
    c = sqrt(x^2+y^2);
    gamma = atan2(y,x);
    beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
    q1 = gamma - beta;
    elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
    q2 = pi - (elb-q1);
    
    fi_start = [q1,q2];
    
    
    x=xyend(1);y=xyend(2);
    c = sqrt(x^2+y^2);
    gamma = atan2(y,x);
    beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
    q1 = gamma - beta;
    elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
    q2 = pi - (elb-q1);
    
    fi_target = [q1,q2];
    
    
    
    %% calculating initial states etc...
    F=[0 0]; %no external forces acting on handn
    %[stim_start, q_start, gamma_start, lce_start, Fdes, Ttot] = calc_stable_start2(fi_start,F,grav_angle,l);
    stim_start=[0,0];
    disp('calculated equilibrium start... now running OC problem')
    nSteps = 1;
    if lengths(iLength)<.4
        nodeLevels = linspace(10,20,2);
    else
        nodeLevels = linspace(10,70,7);
    end
    for iNode = 1:length(nodeLevels) %LOOP THROUGH 3 DAMPING LEVELS, which seems to help.
        if iNode==1
            tic
        end
        disp(['Round: ',num2str(iNode)])
        
        %%%setup the tomlab problem by defining the
        %%%1 TIME
        %%%2 PHASE
        %%%3 CONTROLS
        %%%4 STATES
        toms t tend;
        tend = shadmehr2016(iLength,2);
        steps=nodeLevels(iNode);
        p = tomPhase('p', t, 0, tend, steps, [], 'spline3');
        setPhase(p);
        fi = tomState('fi',1,2);        % segment angles
        fid = tomState('fid',1,2);      % segment angular velocities
        stim = tomState('stim',1,2);  % jer: torque
        %%%/setup the tomlab problem by defining the
        
        %%% initial guess
        if iNode==1
            %initial guess
            x0 = {
                tend == 1; % WARNING: if T IS AN OPT VARIABLE, then THIS NEEDS TO BE FIRST IN LIST. propt documentation.
                icollocate({
                fi  == vec(interp1([0 tend],[fi_start; fi_target],t))'
                fid  == (fi_target-fi_start)/2;%jdw hack
                })
                icollocate({
                stim  == vec(interp1([0 tend],[0,0; 1,1],t))';
                %         stim   == [vec(interp1([0 time],[4.5; 4.51],t/2))',vec(interp1([0 time],[-4.5; -4.51],t/2+1))']
                })
                };
            %%% otherwise warmstart
        else
            x0 = {
                tend == tendopt %%here we are not solving for this!!
                icollocate({
                fi == fiopt
                fid == fidopt
                })
                
                icollocate({
                stim == stimopt
                })
                };
        end
        
        %%% Boundary constraints
        cbnd = {
            initial({
            fi == fi_start;
            fid == 0;
            stim == stim_start;
            })
            final({fi == fi_target;
            fid == 0;
            dot(fid)==0;
            stim == stim_start;
            })
            };
        
        %%% Box constraints
        cbox = {
                   mcollocate(-pi/2 <= fi(1) <= 3*pi/2)
                   mcollocate(-pi/4 <= fi(2)-fi(1) <= pi)
            mcollocate(-100 <= fid <= 100)
            -100 <=  collocate(stim)  <= 100
            0 <=tend <=10
            };
        %%%
        % ODEs and path constraints via virtual power
        %P = load("params_TOMLAB.mat");% reminder: we did this above on
        %line ~26.
        
        I1 = P.I(1);
        I2 = P.I(2);
        m1 = P.m(1);
        m2 = P.m(2);
        d1 = P.d(1);
        d2 = P.d(2);
        l1 = P.l(1);
        l2 = P.l(2);
        m3 = 2;
        
        Massmat = [ I1 + d1^2*m1 + l1^2*m2 + l1^2*m3, l1*cos(fi(1) - fi(2))*(d2*m2 + l2*m3);
            l1*cos(fi(1) - fi(2))*(d2*m2 + l2*m3),          m2*d2^2 + m3*l2^2 + I2];
        
        F =  [-l1*fid(2)^2*sin(fi(1) - fi(2))*(d2*m2 + l2*m3);
            l1*fid(1)^2*sin(fi(1) - fi(2))*(d2*m2 + l2*m3)];
        
        ceq = collocate({
            dot(fi') == fid'
            Massmat*dot(fid') == F + [stim(1)-stim(2);stim(2)]; %JDW: do not seem to need to worry about damping. 
            });
        
        
        
        %%% Objective
        
        %%% Work cost
        powerSho = stim(1)*fid(1) - stim(2)*fid(1);
        powerElb = stim(2)*fid(2);
        posPowerSho = integrate((powerSho+abs(powerSho))/2);
        posPowerElb = integrate((powerElb+abs(powerElb))/2);
        costPositiveWork = posPowerSho + posPowerElb;
        kMechanicalWorkEfficiencyMargaria = 4.2;
        costPositiveWork = costPositiveWork*kMechanicalWorkEfficiencyMargaria;
        
        %%% Force rate cost
        nnshift = 0.001;
        costForceRateLin = cForceRateRate*sum(integrate(sqrt((dot(dot(stim'))).^2+nnshift)));
        costForceRateQuad = cForceRateRate*sum(integrate((dot(dot(stim'))).^2+nnshift));
        costForceRate = costForceRateQuad;
        
        %% Uno force rate cost
        cUno = cUnoTop;
        cUnoForceRate = cUno*sum(integrate((dot(stim')).^2));
        
        %%% Resting cost
        restingMetRate = 100;%100 Watts.
        costResting = 0;%sqrt(tend^2+0.00001)*restingMetRate;
       
        
        %%% Reward cost
        reward = cReward * sqrt(tend^2+0.00001); %avoiding tend<0?
        
        switch objectiveFunction
            case COST_FR_R
                objective = cObj * (costResting + costForceRate + costPositiveWork + reward);
                coefFigTitle = cForceRateRate;
            case COST_FR_ONLY
                objective = cObj * (costResting + costForceRate + costPositiveWork);
                coefFigTitle = cForceRateRate;
            case COST_UNO_R
                objective = cObj * (cUnoForceRate + reward);
                coefFigTitle = cUno;
        
        end
        %% Solve the problem
        options = struct;
        db =dbstack;
        options.name = [nameSim,'--',db.name];
        %       options.scale ='auto';
        
        %%% optimization setup
        options.PriLev = 2;
        options.Prob.SOL.optPar(35) = 100000; %iterations limit
        options.Prob.SOL.optPar(30) = 200000; %major iterations limit
        Prob = sym2prob(objective, {cbox, cbnd, ceq}, x0, options);
        fprintf('now running length %.2f\n',lengths(iLength));
        result = tomRun('snopt',Prob,1);
        solution = getSolution(result);
        fiopt = subs(fi, solution);
        fidopt = subs(fid, solution);
        stimopt = subs(stim, solution);
        tendopt = subs(tend,solution);
    end %end stepLevel loop
    
    %%%add to solution vector
    solutions(iLength) = solution;
    %%%get values
    ECostForceRate(iLength) = subs(costForceRate,solution);
    ECostPositiveWork(iLength) = subs(costPositiveWork,solution);
    ECostResting(iLength) = subs(costResting,solution);
    EReward(iLength) = subs(reward,solution);
    
    tendSol = subs(tend, solution);
    EdotCostForceRate(iLength) = subs(costForceRate,solution)./tendSol ;
    EdotCostPositiveWork(iLength) = subs(costPositiveWork,solution)./tendSol ;
    EdotCostResting(iLength) = subs(costResting,solution)./tendSol;
    EdotReward(iLength) = subs(reward,solution)./tendSol;
    times(iLength) = tendSol;
    ExitFlags(iLength) = result.ExitFlag;
    Informs(iLength) = result.Inform;
    fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
    fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
    fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
    fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
    fprintf('TARGET %i OPTIMIZATION COMPLETE\n',iLength);
    fprintf('Fdotdot power = %.4f \n',EdotCostForceRate(iLength));
    fprintf('Avg positive power = %.4f\n',EdotCostPositiveWork(iLength));
    fprintf('Resting power = %.4f\n',EdotCostResting(iLength));
    fprintf('Reward Power = %.4f\n',EdotReward(iLength));
    
    s=[fi(1) fi(2) fid(1) fid(2)];
    ang = 0;%grav_angle*180/pi;
    f_k=result.f_k;
    Exit=[result.ExitFlag result.Inform];
    
    %%
    t_plot = linspace(0,subs(tend,solution),tendSol/0.01)'; % Time vector
    %u_plot = collocate(t);
    % figure(1);
    s_plot = atPoints(t_plot,subs(s,solution));
    stim_u = atPoints(t_plot,subs(stim,solution));
    fi_plot=s_plot(:,[1 2]);
    fip_plot=s_plot(:,[3 4]);
    
    % this loop compute 1. elbow and hand position 2. segment velocities 3.
    % powers.
    for iEnergyLoop =1:length(fip_plot)
        q=fi_plot(iEnergyLoop,:);
        q = q(:);
        qdot = fip_plot(iEnergyLoop,:);
        qdot = qdot(:);
        
        elb=[l1*cos(q(1)),l1*sin(q(1))];
        hand = elb+[l2*cos(q(2)),l2*sin(q(2))];
        elbs(iEnergyLoop,:) = elb;
        hands(iEnergyLoop,:) = hand;
        j1=[ -d1*sin(q(1)), 0;
            d1*cos(q(1)), 0];
        
        j2 = [-l1*sin(q(1)), -d2*sin(q(2));
            l1*cos(q(1)),  d2*cos(q(2))];
        
        j3 =[ -l1*sin(q(1)), -l2*sin(q(2));
            l1*cos(q(1)),  l2*cos(q(2))];
        
        v1(iEnergyLoop,:) = (j1*qdot)';
        v2(iEnergyLoop,:) = (j2*qdot)';
        v3(iEnergyLoop,:) = (j3*qdot)';
        
        
        powerS(iEnergyLoop) = stim_u(iEnergyLoop,1)*qdot(1);
        powerE(iEnergyLoop) = stim_u(iEnergyLoop,2)*(qdot(2)-qdot(1));
        
        segment1Power(iEnergyLoop) = stim_u(iEnergyLoop,1)*qdot(1) - stim_u(iEnergyLoop,2)*qdot(1);
        segment2Power(iEnergyLoop) = stim_u(iEnergyLoop,2)*(qdot(2));
        
        
    end
    eTau = cumtrapz(t_plot,powerS+powerE);
    
    vPeak(iLength) = max(sqrt(sum(v3.^2,2)));

    eK1 = 1/2*m1*v1.*v1;
    eK2 = 1/2*m2*v2.*v2;
    eK3 = 1/2*m3*v3.*v3;
    ekR1 = 1/2*I1*fip_plot(:,1).^2;
    ekR2 = 1/2*I2*fip_plot(:,2).^2;
    
    eKSum = ekR1 + ekR2 + sum(eK1 + eK2 + eK3,2);
    figure(6);subplot(2,1,1);
    plot(t_plot,eTau);hold on;
    plot(t_plot,eKSum);
    legend({'joint work','kinetic energy'});
    subplot(2,1,2);hold on;
    plot(t_plot,eKSum(:)-eTau(:));
    title('kinetic minus joint work');
    
    fprintf('kinetic energy peak: %.2f \n',max(eKSum));
    
    figure(8); hold on;
    plot(t_plot,sqrt(sum(v3.*v3,2)),'color',iLength/(length(lengths)+1)*[1,1,1]);
    ylabel('Velocity (m/s)'); xlabel('Time (s)');
    figure(9); hold on;
    plot(t_plot,stim_u(:,1),'color',iLength/length(lengths)*[1,1,1]);
    plot(t_plot,stim_u(:,2),'color',iLength/length(lengths)*[1,1,1]);
    ylabel('Torque (ext ref frame)');xlabel('Time (s)');
    tor = atPoints(t_plot,subs(stim,solution));
    t=t_plot;
    state = [fi_plot fip_plot];
    figure(10); hold on;
    plot(t_plot,fi_plot);
    xlabel('Position (m)');xlabel('Time (s)');
    
    % forward simulation.
    parms = struct;
    parms.U(1) = spline(t_plot,stim_u(:,1));
    parms.U(2) = spline(t_plot,stim_u(:,2));
    parm.m3 = m3;
    %   [tSim,stateSim]=ode45(@(tSim,stateSim)forwardMassModel(tSim,stateSim,parms),[0,tendopt],[fi_start,0,0],[]);
end
%% plot forcerate+work
figure(7);
downSample = 10;
for iPos = 1:downSample:length(elbs)
    plot([0,elbs(iPos,1),hands(iPos,1)],[0,elbs(iPos,2),hands(iPos,2)],'color',[1,0,0]*iPos/length(elbs)); hold on;
end
axis equal;
xlabel('x (m)');ylabel('y (m)');

figure(11);
plot(lengths,EdotCostPositiveWork,'marker','.','markersize',20);hold on;
plot(lengths,EdotCostForceRate,'marker','.','markersize',20);
plot(lengths,EdotCostForceRate+EdotCostPositiveWork,'marker','.','markersize',20);
legend('work','frate','net');

ylabel('Power (W)');
xlabel('Reach distance (m)');

% ahmed's prediction is a cost per reach. i.e. J. so plot the J.
figure(12);hold off;
ECostTotal = ECostPositiveWork + ECostForceRate + ECostResting;
plot(lengths,ECostPositiveWork,'marker','.','markersize',20);hold on;
plot(lengths,ECostForceRate,'marker','.','markersize',20);
plot(lengths,ECostResting,'marker','.','markersize',20);
plot(lengths,ECostTotal,'marker','.','markersize',20);

term2Ahmed=0.8*lengths./times.^4.7;
term2Ahmed = term2Ahmed(:);

predAhmed = ECostResting(:) + term2Ahmed;
plot(lengths,predAhmed,'marker','s');
legend('work','frate','resting','net','Ahmed-Pred','Ahmed-Pred-NoResting');

figure(13);
plot(lengths,times,'marker','.','markersize',20);hold on;
ylabel('time');
xlabel('Reach distance (m)');


cd figures
eval(['save ',nameSim,' lengths ECostResting ECostPositiveWork ECostForceRate EReward EdotCostResting EdotCostPositiveWork EdotCostForceRate EdotReward times stim t tendSol steps s fi fid objective f_k Exit solution'])
cd ..

toc
%%
cd figures;
db = dbstack();
fn = db.name;
titleTxt = ['file-',fn,'__',nameSim,' coef=',num2str(coefFigTitle)];
figure(6);
title(titleTxt);
savefig([nameSim,'_energyBalance']);
figure(7);
title(titleTxt);
savefig([nameSim,'_overhead']);
figure(8);
title(titleTxt);
savefig([nameSim,'_dxdt']);
figure(9);
title(titleTxt);
savefig([nameSim,'_u']);
figure(10);
title(titleTxt);
savefig([nameSim,'_x']);
figure(11);
title(titleTxt);
savefig([nameSim,'_energybreakdown']);
title(titleTxt);
figure(12);
title(titleTxt);
savefig([nameSim,'_predAhmed']);
figure(13);
title(titleTxt);
ylim([0,1]);
xlim([0,.6]);
savefig([nameSim,'_distanceTime']);

cd ../